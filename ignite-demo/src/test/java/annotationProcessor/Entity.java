package annotationProcessor;

import com.sbt.entities.abstractAndInterfaces.CacheNameSpecified;
import com.sbt.entities.abstractAndInterfaces.Index;

/**
 * Created by Vladimir Aseev on 31.03.2017.
 */
@CacheNameSpecified(cacheName = "testCacheName")
public class Entity {


    @Index
    private final String s;
    @Index
    private final int i;
    @Index
    private final long l;
    @Index
    private final Integer ii;
    @Index
    private final Long ll;

    private final String ss;

    public Entity(String s, int i, long l, Integer ii, String ss, Long ll) {
        this.s = s;
        this.i = i;
        this.l = l;
        this.ii = ii;
        this.ss = ss;
        this.ll = ll;
    }

    public String getS() {
        return s;
    }

    public int getI() {
        return i;
    }

    public long getL() {
        return l;
    }

    public Integer getIi() {
        return ii;
    }

    public Long getLl() {
        return ll;
    }

    public String getSs() {
        return ss;
    }

}
