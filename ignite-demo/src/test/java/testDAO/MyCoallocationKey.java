package testDAO;

import org.apache.ignite.cache.affinity.AffinityKeyMapped;

/**
 * Created by Vladimir Aseev on 02.04.2017.
 */
public class MyCoallocationKey {

    private static final long PARTITIONS_COUNT = 1023L;

    @AffinityKeyMapped
    private final long clientId;
    private final long entityId;


    public MyCoallocationKey(long clientId, long entityId) {
        this.clientId = clientId;
        this.entityId = entityId;
    }

    public long getClientId() {
        return clientId;
    }

    public long getEntityId() {
        return entityId;
    }
}
