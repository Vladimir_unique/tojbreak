package testDAO;

import com.sbt.dao.accounts.IgniteAccountDAO;
import com.sbt.dao.clients.IgniteClientDAO;
import com.sbt.dao.paymentDocumentParts.IgnitePaymentDocumentPartDAO;
import com.sbt.daoInterfaces.NodeName;
import com.sbt.dto.ClientDTO;
import com.sbt.dto.MethodResult;
import com.sbt.dto.PaymentDocumentPartDTO;
import org.apache.ignite.Ignite;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Created by Vladimir Aseev on 01.04.2017.
 */
public class TestDAO {



    @Test
    public void testCreation(){
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("server-context.xml");

        Ignite ignite = context.getBean(Ignite.class);

        IgnitePaymentDocumentPartDAO documentPartDAO =
                new IgnitePaymentDocumentPartDAO(ignite);

        MethodResult<PaymentDocumentPartDTO> rs0 = documentPartDAO
                .createPaymentDocumentPart("666",
                        0,
                        "test0", true);

        MethodResult<PaymentDocumentPartDTO> rs1 = documentPartDAO
                .createPaymentDocumentPart("666",
                        1,
                        "test1", true);

        MethodResult<PaymentDocumentPartDTO> rs2 = documentPartDAO
                .createPaymentDocumentPart("666",
                        2,
                        "test2", true);

        MethodResult<List<PaymentDocumentPartDTO>> rs4 =
                documentPartDAO.getLocalPaymentDocumentParts("666");


        System.out.println(rs4.getDescription());


/*        ignite.createCache("tttt");

        for(long l = 0L; l < 1025L; l++){
            int partition = ignite.affinity("tttt").partition(new MyCoallocationKey(l, 66L));
            System.out.println("Key: " + l + " Partition: " + partition);
        }*/

/*        IgniteClientDAO clientDAO = new IgniteClientDAO(ignite);
        IgniteAccountDAO accountDAO = new IgniteAccountDAO(ignite);

        MethodResult<ClientDTO> rs = clientDAO.createClient("Vladimir",
                "Aseev",
                "Mikhailovuch",
                true);

        ClientDTO clientFound = rs.getResult();


        System.out.println("id" + clientFound.getId());
        System.out.println(String.format("%s %s %s",
                clientFound.getFirstName(),
                clientFound.getLastName(),
                clientFound.getMiddleName()));*/


    }


}
