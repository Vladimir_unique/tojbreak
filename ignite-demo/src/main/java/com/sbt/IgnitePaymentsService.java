package com.sbt;

import com.sbt.dao.IgniteDAOFactory;
import com.sbt.daoInterfaces.PaymentDocumentDAO;
import com.sbt.daoInterfaces.PaymentDocumentPartDAO;
import com.sbt.distributedTasks.parsing.ParsingTask;
import com.sbt.distributedTasks.payments.PaymentsTask;
import com.sbt.distributedTasks.payments.PaymentsTaskResult;
import com.sbt.distributedTasks.validation.ValidationResult;
import com.sbt.distributedTasks.validation.ValidationTask;
import com.sbt.dto.*;
import com.sbt.services.PaymentsService;
import org.apache.ignite.Ignite;

import java.util.Collection;
import java.util.List;

/**
 * Created by Vladimir Aseev on 01.04.2017.
 *
 * Это имплементация сервиса платежей, которую использует
 * клиент
 */
public class IgnitePaymentsService implements PaymentsService {


    private static final int MAX_PAYMENT_RECORDS_COUNT = 5;
    private static final int STRING_BUILDER_SIZE = 5000;
    private static final String RECORDS_SEPARATOR = "###";

    private final Ignite ignite;
    private final IgniteDAOFactory daoFactory;

    public IgnitePaymentsService(Ignite ignite,
                                 IgniteDAOFactory daoFactory) {
        this.ignite = ignite;
        this.daoFactory = daoFactory;
    }

    /**
     * Задача первоначального сохранения платёжного документа.
     * Первоначальное сохранение платёжного поручения происходит
     * большими частями без парсинга.
     *
     * В демо-проекте это неоптимизированная имитация, в реальности
     * это весьма большой документ на десятки и даже сотни мегабайт
     * текста (номера счетов, фио клиентов и т.п.)
     *
     * @param paymentsDocumentName уникальное имя платёжного документа
     * @param paymentsDocumentBinary тело платёжного документа
     * @return результат выполнения задачи
     */
    @Override
    public MethodResult<PaymentDocumentStatus>
    saveNewPaymentsDocument(String paymentsDocumentName,
                            String paymentsDocumentBinary) {


        try{

            // extract body from outer brackets
            String body =
                    paymentsDocumentBinary
                            .substring(3,
                                    paymentsDocumentBinary.length() - 1);

            // split body to payment records binary
            // очень неэффективно с точки зрения производительности,
            // но для демо пойдёт, а так лучше поменять формат и алгоритм
            String[] rawRecords =
                    body
                            .replace("},", "}},")
                            .split("},");

            // getting DAO from factory
            PaymentDocumentPartDAO documentPartDAO =
                    daoFactory.getPaymentDocumentPartDAO();

            PaymentDocumentDAO paymentDocumentDAO =
                    daoFactory.getPaymentDocumentDAO();

            // calculating blocks count
            int blocksCount = rawRecords.length / MAX_PAYMENT_RECORDS_COUNT;
            if((rawRecords.length % MAX_PAYMENT_RECORDS_COUNT) != 0)
                blocksCount+=1;

            // saving document header
            MethodResult<PaymentDocumentDTO> createdDocument =
                    paymentDocumentDAO.createDocument(paymentsDocumentName,
                    blocksCount,
                    true);

            assert createdDocument
                    .getResultStatus()
                    .equals(MethodResultStatus.SUCCESS);

            // regrouping paymentRecordsIntoBlocks
            int cursor = 0;
            int currentBlockNumber = -1;
            StringBuilder builder =
                    new StringBuilder(STRING_BUILDER_SIZE);
            String[] blocks = new String[blocksCount];
            for(int i = 0; i < rawRecords.length; i++){
                cursor+=1;
                builder.append(rawRecords[i]);
                builder.append(RECORDS_SEPARATOR);
                if(cursor == MAX_PAYMENT_RECORDS_COUNT){
                    cursor = 0;
                    currentBlockNumber+=1;
                    blocks[currentBlockNumber] =
                            builder.toString();
                    builder.setLength(0);
                }
            }
            if(cursor>0){
                currentBlockNumber+=1;
                blocks[currentBlockNumber] =
                        builder.toString();
                builder.setLength(0);
            }


            // saving payment document blocks
            for(int i = 0; i < blocksCount; i++){
                String block = blocks[i];
                MethodResult<PaymentDocumentPartDTO> rs =
                        documentPartDAO
                                .createPaymentDocumentPart(paymentsDocumentName,
                                        i,
                                        block,
                                        true);
                assert rs.getResultStatus()
                        .equals(MethodResultStatus.SUCCESS);
            }

            // updating whole document status

            MethodResult<PaymentDocumentDTO> rsDoc = paymentDocumentDAO
                    .updateStatus(paymentsDocumentName,
                            PaymentDocumentStatus.SAVED,
                            true);

            assert rsDoc
                    .getResultStatus()
                    .equals(MethodResultStatus.SUCCESS);

        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    return MethodResult.ok(PaymentDocumentStatus.SAVED);
    }

    /**
     * Распределённая задача парсинга частей платёжного документа на
     * отдельные записи и клиентоцентричного (записи о платеже клиенту Х
     * лежат на том же физическом узле, что и данные клиента Х)
     * сохранения
     *
     * @param paymentsDocumentName уникальное имя документа,
     *                             который необходимо зачислить
     *
     * @return результат выполнения задачи
     */
    @Override
    public MethodResult<PaymentDocumentStatus>
    parsePaymentsDocument(String paymentsDocumentName) {
        try{

            Collection<MethodResultStatus> response = ignite
                    .compute()
                    .broadcast(new ParsingTask(paymentsDocumentName));

            response.forEach(s ->
            {assert MethodResultStatus.SUCCESS.equals(s);});

            PaymentDocumentDAO paymentDocumentDAO =
                    daoFactory.getPaymentDocumentDAO();

            MethodResult<PaymentDocumentDTO> rs =
                    paymentDocumentDAO.updateStatus(paymentsDocumentName,
                            PaymentDocumentStatus.PARSED,
                    true);

            assert rs
                    .getResultStatus()
                    .equals(MethodResultStatus.SUCCESS);

            return MethodResult.ok(PaymentDocumentStatus.PARSED);

        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    }

    /**
     * Распределённая задача валидации ФИО платёжных записей
     *
     * @param paymentsDocumentName уникальное имя документа,
     *                             который необходимо зачислить
     *
     * @return результат выполнения задачи
     */
    @Override
    public MethodResult<PaymentDocumentStatus> validatePaymentsDocument(String paymentsDocumentName) {
        try{

            Collection<ValidationResult> results = ignite
                    .compute()
                    .broadcast(new ValidationTask(paymentsDocumentName));


            results.forEach(c ->{
                assert (c.equals(ValidationResult.VALIDATED_FULLY) ||
                        c.equals(ValidationResult.VALIDATED_PARTLY));
            });


            PaymentDocumentDAO paymentDocumentDAO =
                    daoFactory.getPaymentDocumentDAO();

            MethodResult<PaymentDocumentDTO> rs =
                    paymentDocumentDAO.updateStatus(paymentsDocumentName,
                    PaymentDocumentStatus.VALIDATED,
                    true);

            assert rs.getResultStatus()
                    .equals(MethodResultStatus.SUCCESS);

            return MethodResult
                    .ok(PaymentDocumentStatus.VALIDATED);

        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }

    }

    /**
     * Распределённая задача зачисления по платёжному документу
     *
     * @param paymentsDocumentName уникальное имя документа,
     *                             который необходимо зачислить
     *
     * @return результат выполнения задачи
     */
    @Override
    public MethodResult<PaymentDocumentStatus> startPaymentProcessForDocument(String paymentsDocumentName) {
        try{

            Collection<PaymentsTaskResult> broadcast = ignite
                    .compute()
                    .broadcast(new PaymentsTask(paymentsDocumentName));

            broadcast.forEach(c -> {
                assert !c.equals(PaymentsTaskResult.ERROR);
            });


            daoFactory
                    .getPaymentDocumentDAO().updateStatus(paymentsDocumentName,
                PaymentDocumentStatus.PAYMENTS_FINISHED,
                true);

        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }

        return MethodResult.ok(PaymentDocumentStatus.PAYMENTS_FINISHED);
    }
}
