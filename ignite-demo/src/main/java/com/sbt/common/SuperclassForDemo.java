package com.sbt.common;

import java.util.Scanner;

/**
 * Created by Vladimir Aseev on 07.03.2017.
 */
public class SuperclassForDemo {

    protected static void waitUserInputAndBlock(){
        System.err.println("Input any data to stop...");
        try{
            Scanner sc = new Scanner(System.in);
            while(!sc.hasNext()){
                Thread.sleep(50L);
            }
        } catch (Exception e){
            return;
        }
        System.err.println("Stopping...");
    }


}
