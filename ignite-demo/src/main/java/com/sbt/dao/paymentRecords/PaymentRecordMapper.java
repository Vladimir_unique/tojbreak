package com.sbt.dao.paymentRecords;

import com.sbt.daoInterfaces.NodeName;
import com.sbt.dto.PaymentRecordDTO;
import com.sbt.entities.PaymentRecordDSO;
import com.sbt.entities.abstractAndInterfaces.DTOEntityMapper;
import com.sbt.entities.affinity.CollocatedWithClient;

/**
 * Created by Vladimir Aseev on 01.04.2017.
 */
public class PaymentRecordMapper
        extends DTOEntityMapper<PaymentRecordDSO,
        PaymentRecordDTO, CollocatedWithClient<Long>> {

    @Override
    public PaymentRecordDTO mapToDTO(PaymentRecordDSO dsoObject,
                                     NodeName storageNodeName) {
        return new PaymentRecordDTO(dsoObject.getKey().getEntityId(),
                dsoObject.getPaymentDocumentName(),
                dsoObject.getKey().getClientId(),
                dsoObject.getPaymentAccountNumber(),
                dsoObject.getClientFirstName(),
                dsoObject.getClientLastName(),
                dsoObject.getClientMiddleName(),
                dsoObject.getPaymentAmount(),
                storageNodeName,
                dsoObject.getStatus());
    }

    @Override
    public PaymentRecordDSO mapToDSO(PaymentRecordDTO dtoObject,
                                     CollocatedWithClient<Long> key) {
        return new PaymentRecordDSO(key,
                dtoObject.getPaymentDocumentName(),
                dtoObject.getAccountNumber(),
                dtoObject.getClientFirstName(),
                dtoObject.getClientLastName(),
                dtoObject.getClientMiddleName(),
                dtoObject.getPaymentAmount(),
                dtoObject.getProcessingStatus());
    }
}
