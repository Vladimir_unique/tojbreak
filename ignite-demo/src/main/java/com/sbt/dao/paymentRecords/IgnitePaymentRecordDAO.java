package com.sbt.dao.paymentRecords;

import com.sbt.daoInterfaces.PaymentRecordDAO;
import com.sbt.dto.MethodResult;
import com.sbt.dto.PaymentRecordDTO;
import com.sbt.dto.PaymentRecordProcessingStatus;
import com.sbt.entities.PaymentRecordDSO;
import com.sbt.entities.abstractAndInterfaces.AbstractEntityDAO;
import com.sbt.entities.abstractAndInterfaces.AnnotationProcessor;
import com.sbt.entities.affinity.CollocatedWithClient;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteAtomicLong;
import org.apache.ignite.cluster.ClusterNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vladimir Aseev on 01.04.2017.
 */
public class IgnitePaymentRecordDAO
        extends AbstractEntityDAO<PaymentRecordDSO, CollocatedWithClient<Long>,
        PaymentRecordMapper, PaymentRecordDTO>
        implements PaymentRecordDAO {

    private final IgniteAtomicLong idGenerator;

    public IgnitePaymentRecordDAO(Ignite ignite) {
        super(ignite,
                PaymentRecordDSO.class,
                new PaymentRecordMapper());
        idGenerator = ignite.atomicLong(AnnotationProcessor
                        .getMainCacheNameForClass(PaymentRecordDSO.class),
                Long.MIN_VALUE,
                true);
    }

    @Override
    public MethodResult<PaymentRecordDTO> createRecord(String paymentDocumentName,
                                                       long clientId,
                                                       String accountNumber,
                                                       String clientFirstName,
                                                       String clientLastName,
                                                       String clientMiddleName,
                                                       long paymentAmount,
                                                       boolean openNewTransaction) {
        try{

            long newId = idGenerator.incrementAndGet();

            CollocatedWithClient<Long> affinity =
                    new CollocatedWithClient<>(clientId, newId);

            PaymentRecordDTO creatingRecord =
                    new PaymentRecordDTO(newId,
                            paymentDocumentName,
                            clientId,
                            accountNumber,
                            clientFirstName,
                            clientLastName,
                            clientMiddleName,
                            paymentAmount,
                            getStorageNodeName(affinity),
                            PaymentRecordProcessingStatus.PARSED);

            create(creatingRecord, affinity);

            return MethodResult.ok(creatingRecord);


        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    }

    @Override
    public MethodResult<List<PaymentRecordDTO>>
    getByPaymentDocumentName(String paymentDocumentName, boolean openNewTransaction) {
        try{
            return MethodResult.ok(getByGlobalIndex("paymentDocumentName",
                    paymentDocumentName));
        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    }

    @Override
    public MethodResult<PaymentRecordDTO> updateProcessingStatus(String paymentDocumentName,
                                                                 String accountNumber,
                                                                 PaymentRecordProcessingStatus newStatus,
                                                                 boolean openNewTransaction) {
        try{
            PaymentRecordDTO oldRecord =
                    getByGlobalIndex("documentNameAndAccountNumber",
                    paymentDocumentName
                            + AbstractEntityDAO.COMPOSITE_INDEX_VALUE_SEPARATOR
                            + accountNumber).get(0);

            CollocatedWithClient<Long> affinity =
                    new CollocatedWithClient<>(oldRecord.getClientId(),
                            oldRecord.getId());

            PaymentRecordDTO updatedRecord =
                    new PaymentRecordDTO(oldRecord.getId(),
                            paymentDocumentName,
                            oldRecord.getClientId(),
                            accountNumber,
                            oldRecord.getClientFirstName(),
                            oldRecord.getClientLastName(),
                            oldRecord.getClientMiddleName(),
                            oldRecord.getPaymentAmount(),
                            oldRecord.getStorageNodeName(),
                            newStatus);

            boolean result =
                    update(oldRecord, updatedRecord, affinity);

            if(result){
                return MethodResult.ok(updatedRecord);
            } else {
                return MethodResult.onError("Could not update payment record status " +
                " for payment record with document name " + paymentDocumentName +
                " and account number " + accountNumber);
            }
        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    }

    @Override
    public MethodResult<List<PaymentRecordDTO>> getAll(boolean openNewTransaction) {
        try{
            return MethodResult.ok(getAll());
        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    }

    @Override
    public MethodResult<List<PaymentRecordDTO>> getLocalPaymentRecords(String paymentDocumentName,
                                                                       boolean openNewTransaction) {
        try{
            ClusterNode clusterNode =
                    ignite.cluster().localNode();

            int[] partitions = ignite
                    .affinity(getMainCacheName())
                    .primaryPartitions(clusterNode);

            List<PaymentRecordDTO> result =
                    new ArrayList<>();

            for(int partition : partitions){
                List<PaymentRecordDSO> byLocalIndex =
                        getByLocalIndex(partition,
                                "paymentDocumentName",
                                paymentDocumentName);
                for(PaymentRecordDSO dso : byLocalIndex){
                    result.add(mapper
                            .mapToDTO(dso,
                                    getStorageNodeName(dso.getKey())));
                }
            }

            return MethodResult.ok(result);
        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    }
}
