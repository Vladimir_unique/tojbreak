package com.sbt.dao.accounts;

import com.sbt.daoInterfaces.NodeName;
import com.sbt.dto.AccountDTO;
import com.sbt.entities.AccountDSO;
import com.sbt.entities.abstractAndInterfaces.DTOEntityMapper;
import com.sbt.entities.affinity.CollocatedWithClient;

/**
 * Created by Vladimir Aseev on 01.04.2017.
 */
public class AccountMapper
        extends DTOEntityMapper<AccountDSO,
        AccountDTO,
        CollocatedWithClient<Long>> {


    @Override
    public AccountDTO mapToDTO(AccountDSO dsoObject, NodeName storageNodeName) {
        return new AccountDTO(dsoObject.getAccountId(),
                dsoObject.getClientId(),
                dsoObject.getAccountNumber(),
                dsoObject.getFunds(),
                storageNodeName);
    }

    @Override
    public AccountDSO mapToDSO(AccountDTO dtoObject, CollocatedWithClient<Long> key) {
        return new AccountDSO(key,
                dtoObject.getClientId(),
                dtoObject.getAccountNumber(),
                dtoObject.getBalance());
    }
}
