package com.sbt.dao.accounts;

import com.sbt.daoInterfaces.AccountDAO;
import com.sbt.dto.AccountDTO;
import com.sbt.dto.MethodResult;
import com.sbt.entities.AccountDSO;
import com.sbt.entities.abstractAndInterfaces.AbstractEntityDAO;
import com.sbt.entities.abstractAndInterfaces.AnnotationProcessor;
import com.sbt.entities.affinity.CollocatedWithClient;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteAtomicLong;

import java.util.List;

/**
 * Created by Vladimir Aseev on 01.04.2017.
 */
public class IgniteAccountDAO
        extends AbstractEntityDAO<AccountDSO,
        CollocatedWithClient<Long>,
        AccountMapper,
        AccountDTO>
        implements AccountDAO {

    private final IgniteAtomicLong idGenerator;

    public IgniteAccountDAO(Ignite ignite) {
        super(ignite,
                AccountDSO.class,
                new AccountMapper());

        idGenerator = ignite.atomicLong(AnnotationProcessor
                        .getMainCacheNameForClass(AccountDSO.class),
                Long.MIN_VALUE,
                true);
    }

    @Override
    public MethodResult<AccountDTO>
    createAccount(String accountNumber,
                  long clientId,
                  boolean openNewTransaction) {
        try{
            long newId =
                    idGenerator.incrementAndGet();

            CollocatedWithClient<Long> key =
                    new CollocatedWithClient<>(clientId,
                            newId);

            AccountDTO creatingAccount =
                    new AccountDTO(newId,
                            clientId,
                            accountNumber,
                            0L,
                            getStorageNodeName(key));

            create(creatingAccount, key);

            return MethodResult.ok(creatingAccount);

        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    }

    @Override
    public MethodResult<List<AccountDTO>> getAccountsByClientId(long clientId,
                                                         boolean openNewTransaction) {
        try{
            return MethodResult.ok(getByGlobalIndex("clientId",
                    clientId));
        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    }

    @Override
    public MethodResult<AccountDTO> getAccountByNumber(String accountNumber,
                                                       boolean openNewTransaction) {
        try{

            List<AccountDTO> accounts =
                    getByGlobalIndex("accountNumber",
                            accountNumber);
            if(accounts.size() == 0) {
                return MethodResult.onNotFound("Account "
                        + accountNumber +
                        " not found!");
            }
            return MethodResult.ok(accounts.get(0));
        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    }

    @Override
    public MethodResult<List<AccountDTO>> getAll(boolean openNewTransaction) {
        try{
            return MethodResult.ok(getAll());
        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    }

    @Override
    public MethodResult<AccountDTO> income(String accountNumber,
                                           long amount,
                                           boolean openNewTransaction) {
        try{
            List<AccountDTO> accounts =
                    getByGlobalIndex("accountNumber", accountNumber);
            AccountDTO accountOld = accounts.get(0);

            AccountDTO accountNew = new AccountDTO(accountOld.getId(),
                    accountOld.getClientId(),
                    accountNumber,
                    accountOld.getBalance() + amount,
                    accountOld.getStorageNodeName());

            update(accountOld,
                    accountNew,
                    new CollocatedWithClient<>(accountOld.getClientId(),
                    accountOld.getId()));

            return MethodResult.ok(accountOld);
        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    }
}
