package com.sbt.dao.clients;

import com.sbt.daoInterfaces.NodeName;
import com.sbt.dto.ClientDTO;
import com.sbt.entities.ClientDSO;
import com.sbt.entities.abstractAndInterfaces.DTOEntityMapper;
import com.sbt.entities.affinity.ClientKey;

/**
 * Created by Vladimir Aseev on 01.04.2017.
 */
public class ClientsMapper extends DTOEntityMapper<ClientDSO, ClientDTO, ClientKey> {


    @Override
    public ClientDTO mapToDTO(ClientDSO dsoObject,
                              NodeName storageNodeName) {
        return new ClientDTO(dsoObject.getKey().getClientId(),
                dsoObject.getFirstName(),
                dsoObject.getLastName(),
                dsoObject.getMiddleName(),
                storageNodeName);
    }

    @Override
    public ClientDSO mapToDSO(ClientDTO dtoObject, ClientKey key) {
        return new ClientDSO(key.getClientId(),
                dtoObject.getFirstName(),
                dtoObject.getLastName(),
                dtoObject.getMiddleName());
    }
}
