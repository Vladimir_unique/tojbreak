package com.sbt.dao.clients;

import com.sbt.daoInterfaces.ClientDAO;
import com.sbt.dto.ClientDTO;
import com.sbt.dto.MethodResult;
import com.sbt.entities.ClientDSO;
import com.sbt.entities.abstractAndInterfaces.AbstractEntityDAO;
import com.sbt.entities.abstractAndInterfaces.AnnotationProcessor;
import com.sbt.entities.affinity.ClientKey;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteAtomicLong;

import java.util.List;

/**
 * Created by Vladimir Aseev on 01.04.2017.
 */
public class IgniteClientDAO
        extends AbstractEntityDAO<ClientDSO, ClientKey, ClientsMapper, ClientDTO>
        implements ClientDAO {


    private final IgniteAtomicLong idGenerator;

    public IgniteClientDAO(Ignite ignite) {
        super(ignite, ClientDSO.class, new ClientsMapper());
        idGenerator = ignite.atomicLong(AnnotationProcessor
                .getMainCacheNameForClass(ClientDSO.class),
                Long.MIN_VALUE,
                true);
    }

    @Override
    public MethodResult<ClientDTO> createClient(String firstName,
                                                String lastName,
                                                String middleName,
                                                boolean openNewTransaction) {

        try{
            Long newId =
                    idGenerator.incrementAndGet();

            ClientKey clientKey =
                    new ClientKey(newId);

            ClientDTO creatingClient = new ClientDTO(newId,
                    firstName,
                    lastName,
                    middleName,
                    getStorageNodeName(clientKey));

            super.create(creatingClient,
                    clientKey);
            return MethodResult.ok(creatingClient);

        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }


    }

    @Override
    public MethodResult<List<ClientDTO>>
    getClientByCredentials(String firstName,
                           String lastName,
                           String middleName,
                           boolean openNewTransaction) {

        try{
            return MethodResult.ok(getByGlobalIndex("credentials",
                    firstName
                            + lastName
                            + middleName));

        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    }

    @Override
    public MethodResult<ClientDTO> getClientById(long clientId) {
        try{
            return MethodResult.ok(getByKey(new ClientKey(clientId)));
        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    }

    @Override
    public MethodResult<List<ClientDTO>> getAll(boolean openNewTransaction) {
        try{
            return MethodResult.ok(getAll());
        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    }

}
