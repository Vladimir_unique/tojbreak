package com.sbt.dao.paymentDocumentParts;

import com.sbt.daoInterfaces.PaymentDocumentPartDAO;
import com.sbt.dto.MethodResult;
import com.sbt.dto.PaymentDocumentPartDTO;
import com.sbt.entities.PaymentDocumentPartDSO;
import com.sbt.entities.abstractAndInterfaces.AbstractEntityDAO;
import com.sbt.entities.abstractAndInterfaces.AnnotationProcessor;
import com.sbt.entities.affinity.PartitionIdResolver;
import com.sbt.entities.affinity.PartitioningKey;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteAtomicLong;
import org.apache.ignite.cluster.ClusterNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vladimir Aseev on 01.04.2017.
 */
public class IgnitePaymentDocumentPartDAO
        extends AbstractEntityDAO<PaymentDocumentPartDSO,
        PartitioningKey,
        PaymentDocumentPartMapper,
        PaymentDocumentPartDTO>
        implements PaymentDocumentPartDAO {


    private final IgniteAtomicLong idGenerator;

    public IgnitePaymentDocumentPartDAO(Ignite ignite) {
        super(ignite,
                PaymentDocumentPartDSO.class,
                new PaymentDocumentPartMapper());
        idGenerator = ignite.atomicLong(AnnotationProcessor
                        .getMainCacheNameForClass(PaymentDocumentPartDSO.class),
                Long.MIN_VALUE,
                true);
    }

    @Override
    public MethodResult<PaymentDocumentPartDTO>
    createPaymentDocumentPart(String paymentDocumentName,
                              int paymentDocumentPartNumber,
                              String paymentDocumentPartData,
                              boolean openNewTransaction) {
        try{

            long newId = idGenerator.incrementAndGet();

            long partitionId =
                    PartitionIdResolver
                            .getPartitionNumberForDocumentPart(paymentDocumentName,
                                    paymentDocumentPartNumber);

            PartitioningKey key = new PartitioningKey(partitionId);

            PaymentDocumentPartDTO creatingDocumentPart =
                    new PaymentDocumentPartDTO(newId,
                            paymentDocumentName,
                            paymentDocumentPartNumber,
                            paymentDocumentPartData,
                            getStorageNodeName(key));

            create(creatingDocumentPart, key);

            return MethodResult.ok(creatingDocumentPart);

        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    }

    @Override
    public MethodResult<PaymentDocumentPartDTO>
    getPaymentDocumentPartByCredentials(String paymentDocumentName,
                                        int paymentDocumentPartNumber,
                                        boolean openNewTransaction) {
        try{
            return MethodResult.ok(getByGlobalIndex("documentCredentials",
                    paymentDocumentName +
            paymentDocumentPartNumber).get(0));
        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    }

    @Override
    public MethodResult<List<PaymentDocumentPartDTO>>
    getPaymentDocumentParts(String paymentDocumentName,
                            boolean openNewTransaction) {
        try{
            return MethodResult.ok(getByGlobalIndex("relatedDocumentName",
                    paymentDocumentName));
        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    }

    @Override
    public MethodResult<List<PaymentDocumentPartDTO>>
    getLocalPaymentDocumentParts(String documentName) {
        try{
            ClusterNode clusterNode =
                    ignite.cluster().localNode();

            int[] partitions = ignite
                    .affinity(getMainCacheName())
                    .primaryPartitions(clusterNode);

            List<PaymentDocumentPartDTO> result =
                    new ArrayList<>();

            for(int partition : partitions){
                List<PaymentDocumentPartDSO> byLocalIndex =
                        getByLocalIndex(partition,
                                "relatedDocumentName",
                                documentName);
                for(PaymentDocumentPartDSO dso : byLocalIndex){
                    result.add(mapper
                            .mapToDTO(dso,
                                    getStorageNodeName(dso.getKey())));
                }
            }

            return MethodResult.ok(result);
        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    }
}
