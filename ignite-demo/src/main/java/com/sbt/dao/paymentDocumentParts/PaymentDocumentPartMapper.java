package com.sbt.dao.paymentDocumentParts;

import com.sbt.daoInterfaces.NodeName;
import com.sbt.dto.PaymentDocumentPartDTO;
import com.sbt.entities.PaymentDocumentPartDSO;
import com.sbt.entities.abstractAndInterfaces.DTOEntityMapper;
import com.sbt.entities.affinity.PartitionIdResolver;
import com.sbt.entities.affinity.PartitioningKey;

/**
 * Created by Vladimir Aseev on 01.04.2017.
 */
public class PaymentDocumentPartMapper
        extends DTOEntityMapper<PaymentDocumentPartDSO, PaymentDocumentPartDTO, PartitioningKey> {

    @Override
    public PaymentDocumentPartDTO mapToDTO(PaymentDocumentPartDSO dsoObject,
                                       NodeName storageNodeName) {
        return new PaymentDocumentPartDTO(
                PartitionIdResolver
                        .getPartitionNumberForDocumentPart(
                                dsoObject
                                        .getRelatedDocumentName(),
                                dsoObject
                                        .getPaymentDocumentPartNumber()),
                dsoObject.getRelatedDocumentName(),
                dsoObject.getPaymentDocumentPartNumber(),
                dsoObject.getDocumentPartData(),
                storageNodeName);
    }

    @Override
    public PaymentDocumentPartDSO mapToDSO(PaymentDocumentPartDTO dtoObject,
                                           PartitioningKey key) {
        return new PaymentDocumentPartDSO(dtoObject.getPaymentDocumentName(),
                dtoObject.getPaymentDocumentPartNumber(),
                dtoObject.getDocumentPartData());
    }
}
