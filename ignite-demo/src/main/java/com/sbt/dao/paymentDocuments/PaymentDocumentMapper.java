package com.sbt.dao.paymentDocuments;

import com.sbt.daoInterfaces.NodeName;
import com.sbt.dto.PaymentDocumentDTO;
import com.sbt.entities.PaymentDocumentDSO;
import com.sbt.entities.abstractAndInterfaces.DTOEntityMapper;
import com.sbt.entities.affinity.PartitioningKey;
import com.sbt.entities.affinity.PaymentDocumentsDataKey;

/**
 * Created by Vladimir Aseev on 01.04.2017.
 */
public class PaymentDocumentMapper extends DTOEntityMapper<PaymentDocumentDSO,
        PaymentDocumentDTO,
        PaymentDocumentsDataKey<String>> {


    @Override
    public PaymentDocumentDTO mapToDTO(PaymentDocumentDSO dsoObject,
                                       NodeName storageNodeName) {
        return new PaymentDocumentDTO(dsoObject.getDocumentName(),
                dsoObject.getStatus(),
                storageNodeName,
                dsoObject.getDocumentPartsCount());
    }

    @Override
    public PaymentDocumentDSO mapToDSO(PaymentDocumentDTO dtoObject,
                                       PaymentDocumentsDataKey<String> key) {
        return new PaymentDocumentDSO(dtoObject.getDocumentName(),
                dtoObject.getStatus(),
                dtoObject.getDocumentPartsCount());
    }
}
