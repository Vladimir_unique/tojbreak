package com.sbt.dao.paymentDocuments;

import com.sbt.daoInterfaces.PaymentDocumentDAO;
import com.sbt.dto.MethodResult;
import com.sbt.dto.PaymentDocumentDTO;
import com.sbt.dto.PaymentDocumentStatus;
import com.sbt.entities.PaymentDocumentDSO;
import com.sbt.entities.abstractAndInterfaces.AbstractEntityDAO;
import com.sbt.entities.affinity.PartitionIdResolver;
import com.sbt.entities.affinity.PaymentDocumentsDataKey;
import org.apache.ignite.Ignite;

import java.util.List;

/**
 * Created by Vladimir Aseev on 01.04.2017.
 */
public class IgnitePaymentDocumentDAO
        extends AbstractEntityDAO<PaymentDocumentDSO,
        PaymentDocumentsDataKey<String>,
        PaymentDocumentMapper,
        PaymentDocumentDTO>
        implements PaymentDocumentDAO {


    //private final IgniteAtomicLong idGenerator;

    public IgnitePaymentDocumentDAO(Ignite ignite) {
        super(ignite,
                PaymentDocumentDSO.class,
                new PaymentDocumentMapper());
/*        idGenerator = ignite.atomicLong(AnnotationProcessor
                        .getMainCacheNameForClass(PaymentDocumentDSO.class),
                Long.MIN_VALUE,
                true);*/
    }

    @Override
    public MethodResult<PaymentDocumentDTO>
    createDocument(String documentName,
                   int documentPartsCount,
                   boolean openNewTransaction) {
        try{

/*            long newId =
                    idGenerator.incrementAndGet();*/

            long partitionId = PartitionIdResolver
                    .getPartitionNumberForDocument(documentName);

            PaymentDocumentsDataKey key =
                    new PaymentDocumentsDataKey<>(documentName, partitionId);

            PaymentDocumentDTO creatingDocument =
                    new PaymentDocumentDTO(documentName,
                            PaymentDocumentStatus.NEW,
                            getStorageNodeName(key),
                            documentPartsCount);

            create(creatingDocument,
                    key);

            return MethodResult.ok(creatingDocument);

        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }


    }

    @Override
    public MethodResult<PaymentDocumentDTO>
    getByName(String documentName, boolean openNewTransaction) {
        try{
            List<PaymentDocumentDTO> documents =
                    getByGlobalIndex("documentName", documentName);
            if(documents.size() == 0){
                return MethodResult.onNotFound("Payment document with name " +
                documentName + " not found.");
            }
            return MethodResult.ok(documents.get(0));
        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    }

    @Override
    public MethodResult<List<PaymentDocumentDTO>> getByStatus(PaymentDocumentStatus status,
                                                              boolean openNewTransaction) {
        try{
            List<PaymentDocumentDTO> paymentDocuments =
                    getByGlobalIndex("status", status);
            if(paymentDocuments.size() == 0){
                return MethodResult.onNotFound("Payment documents with status " +
                status.name() + " not found.");
            }
            return MethodResult.ok(paymentDocuments);
        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    }

    @Override
    public MethodResult<PaymentDocumentDTO> updateStatus(String documentName,
                                                         PaymentDocumentStatus newStatus,
                                                         boolean openNewTransaction) {
        try{
            List<PaymentDocumentDTO> documents =
                    getByGlobalIndex("documentName", documentName);

            PaymentDocumentDTO documentOld =
                    documents.get(0);

            PaymentDocumentDTO documentUpdated =
                    new PaymentDocumentDTO(documentName,
                            newStatus,
                            documentOld
                                    .getStorageNodeName(),
                            documentOld.getDocumentPartsCount());

            long partitionId = PartitionIdResolver
                    .getPartitionNumberForDocument(documentName);

            boolean result = update(documentOld,
                    documentUpdated,
                    new PaymentDocumentsDataKey<>(documentName,
                            partitionId));
            if(result){
                return MethodResult.ok(documentUpdated);
            } else {
                return MethodResult.onError("Could not update document with name " +
                documentName);
            }
        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    }

    @Override
    public MethodResult<List<PaymentDocumentDTO>> getAll(boolean openNewTransaction) {
        try{
            return MethodResult.ok(getAll());
        } catch (Exception e){
            return MethodResult.onError(e.getMessage());
        }
    }
}
