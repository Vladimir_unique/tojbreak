package com.sbt.dao;

import com.sbt.dao.accounts.IgniteAccountDAO;
import com.sbt.dao.clients.IgniteClientDAO;
import com.sbt.dao.paymentDocumentParts.IgnitePaymentDocumentPartDAO;
import com.sbt.dao.paymentDocuments.IgnitePaymentDocumentDAO;
import com.sbt.dao.paymentRecords.IgnitePaymentRecordDAO;
import com.sbt.daoInterfaces.*;
import org.apache.ignite.Ignite;

/**
 * Created by Vladimir Aseev on 30.03.2017.
 */
public class IgniteDAOFactory implements DAOFactory {

    // зарезервировано
    //private final Ignite ignite;
    private final AccountDAO accountDao;
    private final ClientDAO clientDAO;
    private final PaymentDocumentDAO paymentDocumentDAO;
    private final PaymentDocumentPartDAO paymentDocumentPartDAO;
    private final PaymentRecordDAO paymentRecordDAO;

    public IgniteDAOFactory(Ignite ignite){
        // зарезервировано
        //this.ignite = ignite;
        this.accountDao = new IgniteAccountDAO(ignite);
        this.clientDAO = new IgniteClientDAO(ignite);
        this.paymentDocumentDAO = new IgnitePaymentDocumentDAO(ignite);
        this.paymentDocumentPartDAO = new IgnitePaymentDocumentPartDAO(ignite);
        this.paymentRecordDAO = new IgnitePaymentRecordDAO(ignite);
    }


    @Override
    public AccountDAO getAccountDAO() {
        return accountDao;
    }

    @Override
    public ClientDAO getClientDAO() {
        return clientDAO;
    }

    @Override
    public PaymentDocumentDAO getPaymentDocumentDAO() {
        return paymentDocumentDAO;
    }

    @Override
    public PaymentRecordDAO getPaymentRecordDAO() {
        return paymentRecordDAO;
    }

    @Override
    public PaymentDocumentPartDAO getPaymentDocumentPartDAO() {
        return paymentDocumentPartDAO;
    }
}
