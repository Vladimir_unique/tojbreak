package com.sbt.distributedTasks.validation;

import com.sbt.StaticContextHolder;
import com.sbt.daoInterfaces.AccountDAO;
import com.sbt.daoInterfaces.ClientDAO;
import com.sbt.daoInterfaces.DAOFactory;
import com.sbt.daoInterfaces.PaymentRecordDAO;
import com.sbt.dto.*;
import org.apache.ignite.lang.IgniteCallable;

import java.util.List;

/**
 * Created by Vladimir Aseev on 08.04.2017.
 *
 * Это распределённая задача проверки ФИО клиентов
 * (ФИО должно совпадать в платёжке и у клиента заданного
 * аккаунта)
 *
 */
public class ValidationTask implements IgniteCallable<ValidationResult> {


    private final String paymentDocumentName;

    public ValidationTask(String paymentDocumentName) {
        this.paymentDocumentName = paymentDocumentName;
    }


    @Override
    public ValidationResult call() throws Exception {
        try{
            boolean hasErrors = false;
            DAOFactory daoFactory =
                    StaticContextHolder.getDaoFactory();

            PaymentRecordDAO paymentRecordDAO =
                    daoFactory.getPaymentRecordDAO();


            MethodResult<List<PaymentRecordDTO>> rs = paymentRecordDAO
                    .getLocalPaymentRecords(paymentDocumentName,
                            true);

            assert rs.getResultStatus().equals(MethodResultStatus.SUCCESS);

            List<PaymentRecordDTO> localRecords = rs.getResult();

            for(PaymentRecordDTO recordDTO : localRecords){
                try{
                    if(validateRecord(recordDTO, daoFactory)){
                        MethodResult<PaymentRecordDTO> upd =
                                paymentRecordDAO.updateProcessingStatus(paymentDocumentName,
                                        recordDTO.getAccountNumber(),
                                        PaymentRecordProcessingStatus.VALIDATED_OK,
                                        true);

                        assert upd.getResultStatus().equals(MethodResultStatus.SUCCESS);
                    } else {
                        MethodResult<PaymentRecordDTO> upd =
                                paymentRecordDAO.updateProcessingStatus(paymentDocumentName,
                                        recordDTO.getAccountNumber(),
                                        PaymentRecordProcessingStatus.VALIDATED_INVALID,
                                        true);
                        hasErrors = true;

                        assert upd.getResultStatus().equals(MethodResultStatus.SUCCESS);
                    }
                } catch (Exception e){
                    hasErrors = true;
                }

            }


            if(hasErrors){
                return ValidationResult.VALIDATED_PARTLY;
            } else {
                return ValidationResult.VALIDATED_FULLY;
            }
        } catch (Exception e){
            return ValidationResult.ERROR;
        }
    }

    private boolean validateRecord(PaymentRecordDTO record,
                                   DAOFactory daoFactory){
        try{
            AccountDAO accountDAO = daoFactory.getAccountDAO();
            ClientDAO clientDAO = daoFactory.getClientDAO();

            MethodResult<AccountDTO> accountByNumber =
                    accountDAO.getAccountByNumber(record.getAccountNumber(),
                            true);

            assert accountByNumber
                    .getResultStatus()
                    .equals(MethodResultStatus.SUCCESS);

            AccountDTO account = accountByNumber.getResult();

            MethodResult<ClientDTO> clientById =
                    clientDAO
                            .getClientById(account
                                    .getClientId());

            assert clientById
                    .getResultStatus()
                    .equals(MethodResultStatus.SUCCESS);

            ClientDTO client = clientById.getResult();

            assert client.getFirstName().equals(record.getClientFirstName());
            assert client.getLastName().equals(record.getClientLastName());
            assert client.getMiddleName().equals(record.getClientMiddleName());

            return true;

        } catch (Exception e){
            return false;
        }


    }
}
