package com.sbt.distributedTasks.validation;

/**
 * Created by Vladimir Aseev on 08.04.2017.
 */
public enum ValidationResult {

    VALIDATED_FULLY,
    VALIDATED_PARTLY,
    ERROR;

}
