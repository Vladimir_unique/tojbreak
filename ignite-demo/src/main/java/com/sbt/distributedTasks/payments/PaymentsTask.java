package com.sbt.distributedTasks.payments;

import com.sbt.StaticContextHolder;
import com.sbt.daoInterfaces.DAOFactory;
import com.sbt.daoInterfaces.PaymentRecordDAO;
import com.sbt.dto.*;
import org.apache.ignite.lang.IgniteCallable;

import java.util.List;

/**
 * Created by Vladimir Aseev on 08.04.2017.
 *
 * Это распределённая задача зачисления средств на счета
 */
public class PaymentsTask implements IgniteCallable<PaymentsTaskResult> {



    private final String paymentDocumentName;

    public PaymentsTask(String paymentDocumentName) {
        this.paymentDocumentName = paymentDocumentName;
    }


    @Override
    public PaymentsTaskResult call() throws Exception {
        try{
            DAOFactory daoFactory =
                    StaticContextHolder.getDaoFactory();

            MethodResult<List<PaymentRecordDTO>> localPaymentRecords = daoFactory
                    .getPaymentRecordDAO()
                    .getLocalPaymentRecords(paymentDocumentName,
                            true);

            assert localPaymentRecords
                    .getResultStatus()
                    .equals(MethodResultStatus.SUCCESS);

            boolean hasErrors = false;

            for(PaymentRecordDTO record : localPaymentRecords.getResult()){
                hasErrors |= prepareIncome(record, daoFactory);
            }

            if(hasErrors){
                return PaymentsTaskResult.PROCESSED_PARTLY;
            } else {
                return PaymentsTaskResult.PROCESSED_FULLY;
            }

        } catch (Exception e){
            return PaymentsTaskResult.ERROR;
        }
    }


    private boolean prepareIncome(PaymentRecordDTO paymentRecord,
                                  DAOFactory daoFactory){

        if(!paymentRecord
                .getProcessingStatus()
                .equals(PaymentRecordProcessingStatus.VALIDATED_OK)){
            return false;
        }
        try{

            MethodResult<AccountDTO> income =
                    daoFactory.getAccountDAO().income(paymentRecord.getAccountNumber(),
                    paymentRecord.getPaymentAmount(),
                    true);

            assert income.getResultStatus().equals(MethodResultStatus.SUCCESS);

            MethodResult<PaymentRecordDTO> paymentResult = daoFactory.getPaymentRecordDAO().updateProcessingStatus(paymentDocumentName,
                    paymentRecord.getAccountNumber(),
                    PaymentRecordProcessingStatus.PAYMENT_FINISHED,
                    true);

            assert paymentResult
                    .getResultStatus()
                    .equals(MethodResultStatus.SUCCESS);

            return true;
        } catch (Exception e){
            try{
                MethodResult<PaymentRecordDTO> rs = daoFactory.getPaymentRecordDAO().updateProcessingStatus(paymentDocumentName,
                        paymentRecord.getAccountNumber(),
                        PaymentRecordProcessingStatus.ERROR,
                        true);
            } catch (Exception ee){}
            return false;
        }
    }
}
