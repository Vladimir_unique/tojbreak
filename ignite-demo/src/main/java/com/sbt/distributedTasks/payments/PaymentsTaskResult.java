package com.sbt.distributedTasks.payments;

/**
 * Created by Vladimir Aseev on 08.04.2017.
 */
public enum PaymentsTaskResult {

    PROCESSED_FULLY,
    PROCESSED_PARTLY,
    ERROR;

}
