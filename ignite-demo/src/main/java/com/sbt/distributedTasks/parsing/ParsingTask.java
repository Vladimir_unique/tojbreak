package com.sbt.distributedTasks.parsing;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sbt.StaticContextHolder;
import com.sbt.daoInterfaces.AccountDAO;
import com.sbt.daoInterfaces.DAOFactory;
import com.sbt.daoInterfaces.PaymentRecordDAO;
import com.sbt.dto.*;
import org.apache.ignite.lang.IgniteCallable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Данный класс представляет собой единицу распределённой работы
 * по "Парсингу" платёжного документа.
 *
 * При запуске распределённой задачи каждый узел кластера загружает себе
 * данный класс средствами peer-class-loader (если опция ignite включена),
 * после чего запускает метод call().
 *
 */
public class ParsingTask implements IgniteCallable<MethodResultStatus> {

    private String paymentsDocumentName;
    private DAOFactory daoFactory;
    private GsonBuilder gsonBuilder;

    public ParsingTask(String paymentsDocumentName) {
        this.paymentsDocumentName = paymentsDocumentName;
    }

    /**
     * Исполняемый метод распределённой задачи "Парсинга"
     * частей платёжного документа на отдельные записи
     *
     * @return статус выполнения задачи на текущем узле.
     * на reduce шаге задачи при типе исполнения
     * broadcast ignite соберет
     * эти статусы выполнения со всех узлов кластера и
     * вернёт коллекцию статусов тому, кто вызвал распределённую задачу
     */
    @Override
    public MethodResultStatus call() {

    try{
        daoFactory = StaticContextHolder.getDaoFactory();
        gsonBuilder = new GsonBuilder();
        PaymentDocumentDTO doc = getDocument();

        List<PaymentDocumentPartDTO> parts =  daoFactory
                .getPaymentDocumentPartDAO()
                .getLocalPaymentDocumentParts(doc.getDocumentName()).getResult();



        if(parts == null){
            parts = new ArrayList<>();
        }

        AccountDAO accountDAO = daoFactory.getAccountDAO();

        Map<Long, RawPaymentRecordDTO> records =
                new HashMap<>();

        for(PaymentDocumentPartDTO part : parts) {
            List<RawPaymentRecordDTO> recordsFromPart =
                    parsePart(part);

            for(RawPaymentRecordDTO record : recordsFromPart){
                MethodResult<AccountDTO> accountByNumber = accountDAO
                        .getAccountByNumber(record.getAccountNumber(),
                                true);

                assert accountByNumber
                        .getResultStatus()
                        .equals(MethodResultStatus.SUCCESS);

                records.put(accountByNumber.getResult().getClientId(),
                        record);
            }

        }

        saveParsedRecords(records);
    } catch (Exception e){
        return MethodResultStatus.ERROR;
    }
        return MethodResultStatus.SUCCESS;
    }


    private void saveParsedRecords(Map<Long, RawPaymentRecordDTO> recordsAndClientIds) {
        PaymentRecordDAO paymentRecordDAO =
                daoFactory.getPaymentRecordDAO();

        for(Map.Entry<Long, RawPaymentRecordDTO> entry : recordsAndClientIds.entrySet()){
            MethodResult<PaymentRecordDTO> rs =
                    paymentRecordDAO.createRecord(paymentsDocumentName,
                    entry.getKey(),
                    entry.getValue().getAccountNumber(),
                    entry.getValue().getClientFirstName(),
                    entry.getValue().getClientLastName(),
                    entry.getValue().getClientMiddleName(),
                    Math.round(entry.getValue().getPaymentAmount()),
                    true);

            assert rs.getResultStatus()
                    .equals(MethodResultStatus.SUCCESS);
        }

    }

    private List<RawPaymentRecordDTO> parsePart(PaymentDocumentPartDTO part) {
        List<RawPaymentRecordDTO> result = new ArrayList<>();
        String stringPart = part.getDocumentPartData();
        String[] stringPartArray = stringPart.split("###");
        Gson gson = gsonBuilder.create();
        for(String json : stringPartArray) {
            RawPaymentRecordDTO dto = gson.fromJson(json, RawPaymentRecordDTO.class);
            result.add(dto);
        }
        return result;
    }

    private PaymentDocumentDTO getDocument() {
        MethodResult<PaymentDocumentDTO> resultDoc = daoFactory.getPaymentDocumentDAO().getByName(paymentsDocumentName, false);
        if (resultDoc.getResultStatus() != MethodResultStatus.SUCCESS) {
            throw new IllegalStateException(String.format("Document %s not found", paymentsDocumentName));
        }
        return resultDoc.getResult();
    }


}