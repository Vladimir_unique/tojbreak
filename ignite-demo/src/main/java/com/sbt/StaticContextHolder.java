package com.sbt;

import com.sbt.dao.IgniteDAOFactory;
import com.sbt.daoInterfaces.DAOFactory;
import org.apache.ignite.Ignite;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Vladimir Aseev on 04.04.2017.
 */
public class StaticContextHolder {


    private static final ClassPathXmlApplicationContext context;
    private static final Ignite ignite;
    private static final DAOFactory daoFactory;

    static {
        context =
                new ClassPathXmlApplicationContext("server-context.xml");
        ignite = context.getBean(Ignite.class);
        daoFactory = new IgniteDAOFactory(ignite);
    }

    public static ClassPathXmlApplicationContext getContext() {
        return context;
    }

    public static Ignite getIgnite() {
        return ignite;
    }

    public static DAOFactory getDaoFactory() {
        return daoFactory;
    }
}
