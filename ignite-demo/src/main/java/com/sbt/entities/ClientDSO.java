package com.sbt.entities;

import com.sbt.entities.abstractAndInterfaces.AbstractDataEntity;
import com.sbt.entities.abstractAndInterfaces.CacheNameSpecified;
import com.sbt.entities.abstractAndInterfaces.CompositeIndices;
import com.sbt.entities.abstractAndInterfaces.CompositeIndex;
import com.sbt.entities.affinity.ClientKey;

/**
 * Created by Vladimir Aseev on 11.03.2017.
 */
@CompositeIndices(compositeIndices = {
        @CompositeIndex(name = "credentials" , fields = {
                "firstName",
                "lastName",
                "middleName"
        })
})
@CacheNameSpecified(cacheName = "clientRelatedData")
public final class ClientDSO extends AbstractDataEntity<ClientKey> {

    private final String firstName;
    private final String lastName;
    private final String middleName;

    public ClientDSO(long clientId,
                     String firstName,
                     String lastName,
                     String middleName) {
        super(new ClientKey(clientId));
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
    }

    @Override
    public String toString() {
        return String.format("ClientDSO: [id=%d %s %s %s]",
                getKey().getClientId(),
                lastName,
                firstName,
                middleName);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

}
