package com.sbt.entities.abstractAndInterfaces;

/**
 * Created by Vladimir Aseev on 08.04.2017.
 */
public enum IndexKind {

    GLOBAL_ONLY(true, false),
    LOCAL_ONLY(false, true),
    GLOBAL_AND_LOCAL(true, true);


    final boolean hasGlobalIndexing;
    final boolean hasLocalIndexing;

    IndexKind(boolean hasGlobalIndexing,
              boolean hasLocalIndexing){
        this.hasGlobalIndexing = hasGlobalIndexing;
        this.hasLocalIndexing = hasLocalIndexing;
    }

    public boolean isHasGlobalIndexing() {
        return hasGlobalIndexing;
    }

    public boolean isHasLocalIndexing() {
        return hasLocalIndexing;
    }
}
