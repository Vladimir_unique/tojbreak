package com.sbt.entities.abstractAndInterfaces;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Vladimir Aseev on 31.03.2017.
 */
public class AnnotationProcessor {


    /**
     * Создадим информацию по процессингу простого индекса
     * @param index информация о стратегии (аннотация)
     * @param dsoClass хранимый класс
     * @param fieldName имя поля, информация по индексированию
     *                  которого строится
     * @param mainCacheName главный кэш хранимого объекта, откуда
     *                      его можно достать по первичному ключу
     * @return информация по процессингу индекса
     */
    private static IndexProcessingInfo
    createProcessingInfo(Index index,
                            Class<? extends AbstractDataEntity> dsoClass,
                            String fieldName,
                            String mainCacheName){
        String globalIndexCacheName = null;
        String localIndexCacheName = null;
        boolean createGlobalIndex = false;
        boolean createLocalIndex = false;
        IndexKind indexKind =
                index.indexKind();
        if (indexKind.hasGlobalIndexing) {
            globalIndexCacheName =
                    dsoClass.getCanonicalName() +
                            fieldName;
            createGlobalIndex = true;
        }
        if (indexKind.hasLocalIndexing) {
            localIndexCacheName =
                    mainCacheName;
            createLocalIndex = true;
        }
        return new IndexProcessingInfo(createGlobalIndex,
                createLocalIndex,
                localIndexCacheName,
                globalIndexCacheName,
                false);
    }

    /**
     * Создадим информацию по процессингу композитного индекса
     * @param index информация о стратегии (аннотация)
     * @param dsoClass хранимый класс
     * @param mainCacheName главный кэш хранимого объекта, откуда
     *                      его можно достать по первичному ключу
     * @return информация по процессингу композитного индекса
     */
    private static IndexProcessingInfo
    createProcessingInfo(CompositeIndex index,
                         Class<? extends AbstractDataEntity> dsoClass,
                         String mainCacheName) {
        String globalIndexCacheName = null;
        String localIndexCacheName = null;
        boolean createGlobalIndex = false;
        boolean createLocalIndex = false;
        IndexKind indexKind =
                index.indexKind();
        if (indexKind.hasGlobalIndexing) {
            globalIndexCacheName =
                    dsoClass.getCanonicalName() +
                            index.name();
            createGlobalIndex = true;
        }
        if (indexKind.hasLocalIndexing) {
            localIndexCacheName =
                    mainCacheName;
            createLocalIndex = true;
        }
        return new CompositeIndexProcessingInfo(createGlobalIndex,
                createLocalIndex,
                localIndexCacheName,
                globalIndexCacheName,
                Arrays.asList(index.fields()));
    }

    /**
     * Метод создаёт информацию по стратегии обработки для
     * всех индексов хранимого объекта (как простых,
     * так и композитных)
     *
     * @param dsoClass класс объекта хранения
     * @return мапа вида Название_Индекса : Информация_по_обработке_индекса
     */
    public static Map<String, IndexProcessingInfo> getIndicesProcessingInfo
    (Class<? extends AbstractDataEntity> dsoClass) {
        Map<String, IndexProcessingInfo> result = new HashMap<>();
        String mainCacheName = getMainCacheNameForClass(dsoClass);
        // processing indices for fields
        for (Field f : dsoClass.getDeclaredFields()) {
            for (Annotation a : f.getAnnotations()) {
                if (a.annotationType() == Index.class) {
                    Index casted = (Index) a;
                    result.put(f.getName(),
                            createProcessingInfo(casted,
                                    dsoClass,
                                    f.getName(),
                                    mainCacheName));
                }
            }
        }

        // processing indices for class itself
        CompositeIndices compositeIndices =
                dsoClass.getAnnotation(CompositeIndices.class);
        if(compositeIndices != null){
            for (CompositeIndex index : compositeIndices.compositeIndices()) {
                // tiny validation
                if (result.containsKey(index.name())) {
                    throw new RuntimeException("Incorrect composite index name for class: " +
                            dsoClass.getCanonicalName() + ". Selected name " + index.name() +
                            " conflicts with indexed field " + index.name());
                }
                result.put(index.name(), createProcessingInfo(index,
                        dsoClass,
                        mainCacheName));
            }
        }


        return result;
    }


    /**
     * Метод возвращает имя главного кэша для класса,
     * в котором объекты класса доступны по первичному ключу
     * @param entityClass класс хранимых объектов
     * @return имя кэша
     */
    public static String getMainCacheNameForClass(Class entityClass){
        Annotation[] annotations = entityClass.getAnnotations();
        for (Annotation annotation : annotations){
            if(annotation.annotationType() == CacheNameSpecified.class){
                return ((CacheNameSpecified)annotation).cacheName();
            }
        }
        return entityClass.getCanonicalName();
    }





}
