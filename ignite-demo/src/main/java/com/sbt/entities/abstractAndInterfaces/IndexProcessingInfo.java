package com.sbt.entities.abstractAndInterfaces;

/**
 * Created by Vladimir Aseev on 08.04.2017.
 */
public class IndexProcessingInfo {

    private final boolean hasGlobalIndexing;
    private final boolean hasLocalIndexing;
    private final String localIndexCacheName;
    private final String globalIndexCacheName;
    private final boolean compositeIndex;

    public IndexProcessingInfo(boolean hasGlobalIndexing,
                               boolean hasLocalIndexing,
                               String localIndexCacheName,
                               String globalIndexCacheName) {
        this.hasGlobalIndexing = hasGlobalIndexing;
        this.hasLocalIndexing = hasLocalIndexing;
        this.localIndexCacheName = localIndexCacheName;
        this.globalIndexCacheName = globalIndexCacheName;
        this.compositeIndex = false;
    }

    protected IndexProcessingInfo(boolean hasGlobalIndexing,
                               boolean hasLocalIndexing,
                               String localIndexCacheName,
                               String globalIndexCacheName,
                               boolean compositeIndex) {
        this.hasGlobalIndexing = hasGlobalIndexing;
        this.hasLocalIndexing = hasLocalIndexing;
        this.localIndexCacheName = localIndexCacheName;
        this.globalIndexCacheName = globalIndexCacheName;
        this.compositeIndex = compositeIndex;
    }

    public boolean hasGlobalIndexing() {
        return hasGlobalIndexing;
    }

    public boolean hasLocalIndexing() {
        return hasLocalIndexing;
    }

    public String getLocalIndexCacheName() {
        return localIndexCacheName;
    }

    public String getGlobalIndexCacheName() {
        return globalIndexCacheName;
    }

    public boolean isCompositeIndex() {
        return compositeIndex;
    }
}
