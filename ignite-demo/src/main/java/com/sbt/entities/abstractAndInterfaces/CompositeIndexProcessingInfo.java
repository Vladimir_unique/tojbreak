package com.sbt.entities.abstractAndInterfaces;

import java.util.List;

/**
 * Created by Vladimir Aseev on 08.04.2017.
 */
public class CompositeIndexProcessingInfo extends IndexProcessingInfo {

    private final List<String> fieldNames;

    public CompositeIndexProcessingInfo(boolean hasGlobalIndexing,
                                        boolean hasLocalIndexing,
                                        String localIndexCacheName,
                                        String globalIndexCacheName,
                                        List<String> fieldNames) {
        super(hasGlobalIndexing,
                hasLocalIndexing,
                localIndexCacheName,
                globalIndexCacheName,
                true);
        this.fieldNames = fieldNames;
    }

    public List<String> getFieldNames() {
        return fieldNames;
    }
}
