package com.sbt.entities.abstractAndInterfaces;

/**
 * Created by Vladimir Aseev on 30.03.2017.
 */
public abstract class AbstractDataEntity<KEY_TYPE>
        implements Cloneable {


    protected final KEY_TYPE key;


    protected AbstractDataEntity(KEY_TYPE key){
        this.key = key;
    }

    public KEY_TYPE getKey() {
        return key;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractDataEntity<?> that = (AbstractDataEntity<?>) o;

        return key.equals(that.key);
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }

    @Override
    @SuppressWarnings("unchecked")
    public AbstractDataEntity<KEY_TYPE> clone() {
        try {
            return (AbstractDataEntity<KEY_TYPE>) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

}
