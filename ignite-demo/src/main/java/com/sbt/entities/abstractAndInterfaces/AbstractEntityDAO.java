package com.sbt.entities.abstractAndInterfaces;

import com.sbt.daoInterfaces.NodeName;
import com.sbt.entities.affinity.LocalIndex;
import com.sbt.entities.affinity.PartitioningKey;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.cluster.ClusterNode;

import javax.cache.Cache;
import java.lang.reflect.Field;
import java.util.*;

/**
 * Created by Vladimir Aseev on 30.03.2017.
 */
public abstract class AbstractEntityDAO<DSO_TYPE extends AbstractDataEntity<KEY_TYPE>,
KEY_TYPE extends PartitioningKey, MAPPER extends DTOEntityMapper<DSO_TYPE, DTO_TYPE, KEY_TYPE>, DTO_TYPE> {

    public static final String COMPOSITE_INDEX_VALUE_SEPARATOR = "#*&^~#";

    protected final Ignite ignite;
    private final Class<DSO_TYPE> entityClass;
    protected final MAPPER mapper;
    private final Map<String, IndexProcessingInfo> processingInfo;
    private final String mainCacheName;


    protected AbstractEntityDAO(Ignite ignite,
                                Class<DSO_TYPE> entityClass,
                                MAPPER mapper){
        this.ignite = ignite;
        this.entityClass = entityClass;
        this.mapper = mapper;
        this.processingInfo =
                AnnotationProcessor
                        .getIndicesProcessingInfo(entityClass);
        this.mainCacheName =
                AnnotationProcessor
                        .getMainCacheNameForClass(entityClass);
    }




    /**
     * Метод возвращает значения всех индексов
     *
     * @param indicesProcessingInfo информация о том, как индексы надо
     *                              обрабатывать вида
     *                              НазваниеИндекса : Информация об индексе
     * @param obj хранимый объект, для которого надо значения прочесть
     * @return карта вида ИмяИндекса : ЗначениеИндекса
     */
    private Map<String, Object>
    getIndicesValues(Map<String,IndexProcessingInfo> indicesProcessingInfo,
                     Object obj) {
        Map<String, Object> result = new HashMap<>();
        Class dsoClass = obj.getClass();

        // for each index
        for(Map.Entry<String, IndexProcessingInfo> entry :
                indicesProcessingInfo.entrySet()){
            IndexProcessingInfo processingInfo =
                    entry.getValue();

            if(processingInfo.isCompositeIndex()){
                // if composite index
                CompositeIndexProcessingInfo casted =
                        (CompositeIndexProcessingInfo) processingInfo;
                StringBuilder builder = new StringBuilder();

                for(String fieldName : casted.getFieldNames()){
                    try{
                        Field field = dsoClass.getDeclaredField(fieldName);
                        field.setAccessible(true);
                        builder.append(COMPOSITE_INDEX_VALUE_SEPARATOR);
                        builder.append(field.get(obj));


                    } catch (ReflectiveOperationException e) {
                        e.printStackTrace();
                    }

                }

                result.put(entry.getKey(),
                        builder
                                .toString()
                                .substring(COMPOSITE_INDEX_VALUE_SEPARATOR.length()));

            } else {
                // if simple index
                try{
                    Field field =
                            dsoClass.getDeclaredField(entry.getKey());
                    field.setAccessible(true);

                    result.put(entry.getKey(), field.get(obj));

                } catch (ReflectiveOperationException e) {
                    e.printStackTrace();
                }

            }

        }
        return result;
    }


    protected DTO_TYPE getByKey(KEY_TYPE key){

        IgniteCache<KEY_TYPE, DSO_TYPE> mainCache =
                getMainCache();

        return mapper.mapToDTO(mainCache.get(key),
                getStorageNodeName(key));
    }

    protected NodeName getStorageNodeName(KEY_TYPE key){

        ClusterNode node = ignite
                .affinity(mainCacheName)
                .mapKeyToNode(key);

        if(node != null){
            try{
                return node
                        .attribute(NodeName.NODE_COLOR_ATTRIBUTE_NAME);
            } catch (Exception e){
                e.printStackTrace();
                return NodeName.UNKNOWN;
            }
        } else {
            System.err.println("Could not read ignite property " +
                    NodeName.NODE_COLOR_ATTRIBUTE_NAME);
            return NodeName.UNKNOWN;
        }

    }

    protected <T extends KEY_TYPE> void  create(DTO_TYPE value, T key){
        DSO_TYPE insertingValue =
                mapper.mapToDSO(value, key);

        IgniteCache<KEY_TYPE, DSO_TYPE> mainCache =
                getMainCache();

        mainCache.put(key, insertingValue);

        createIndicesForValue(insertingValue);
    }

    protected <T extends KEY_TYPE> boolean remove(DTO_TYPE removingValue,
                             T key){

        DSO_TYPE dso =
                mapper.mapToDSO(removingValue, key);

        removeIndicesForValue(dso);

        IgniteCache<KEY_TYPE, DSO_TYPE> mainCache =
                getMainCache();

        return mainCache.remove(key);

    }

    protected <T extends KEY_TYPE> boolean update(DTO_TYPE oldValue,
                             DTO_TYPE updatedValue,
                             T key){
        DSO_TYPE removingValue =
                mapper.mapToDSO(oldValue, key);

        removeIndicesForValue(removingValue);

        DSO_TYPE creatingValue =
                mapper.mapToDSO(updatedValue, key);

        IgniteCache<KEY_TYPE, DSO_TYPE> mainCache =
                getMainCache();

        createIndicesForValue(creatingValue);

        return mainCache.replace(key, creatingValue);

    }

    protected List<DTO_TYPE> getAllByLocalIndex(String indexName,
                                                LocalIndex<Object, DSO_TYPE> indexValue){

        ClusterNode localNode = ignite.cluster().localNode();

        int[] localPartitions = ignite
                .affinity(mainCacheName)
                .primaryPartitions(localNode);

        List<DTO_TYPE> result = new ArrayList<>();

        IndexProcessingInfo indexInfo = this.processingInfo.get(indexName);

        IgniteCache<Object, List<KEY_TYPE>> indexCache =
                getIgniteCacheByName(indexInfo.getLocalIndexCacheName());

        for(int partitionNumber : localPartitions){

            LocalIndex<Object, DSO_TYPE> localIndexKey =
                    new LocalIndex<>(partitionNumber,
                            indexValue,
                            entityClass);


            IgniteCache<KEY_TYPE, DSO_TYPE> mainCacheCasted =
                    getMainCache();

            if(indexCache.containsKey(localIndexKey)){
                List<KEY_TYPE> primaryKeys =
                        indexCache.get(localIndexKey);
                for(KEY_TYPE primaryKey : primaryKeys){
                    DSO_TYPE dso = mainCacheCasted.get(primaryKey);

                    result.add(
                            mapper.mapToDTO(dso,
                                    getStorageNodeName(dso.key)));
                }

            }

        }

        return result;
    }


    protected List<DSO_TYPE> getByLocalIndex(long partitionId,
                                             String indexName,
                                             Object indexValue){

        String cacheName =
                processingInfo.get(indexName)
                        .getLocalIndexCacheName();

        IgniteCache<Object, List<KEY_TYPE>> cache =
                ignite.getOrCreateCache(cacheName);

        LocalIndex<Object, DSO_TYPE> localIndexKey =
                new LocalIndex<>(partitionId, indexValue, entityClass);

        List<DSO_TYPE> result = new ArrayList<>();

        IgniteCache<KEY_TYPE, DSO_TYPE> mainCache =
                getMainCache();

        if(cache.containsKey(localIndexKey)){
            List<KEY_TYPE> primaryKeys =
                    cache.get(localIndexKey);
            for(KEY_TYPE primaryKey : primaryKeys){
                result.add(mainCache.get(primaryKey));
            }

        }
        return result;
    }

    protected List<DTO_TYPE> getByGlobalIndex(String indexName,
                                              Object indexValue){

        String indexCacheName =
                processingInfo.get(indexName).getGlobalIndexCacheName();

        IgniteCache<Object, List<KEY_TYPE>> indicesCache =
                ignite.getOrCreateCache(indexCacheName);

        List<KEY_TYPE> keys = indicesCache.get(indexValue);
        List<DTO_TYPE> result = new ArrayList<>();
        IgniteCache<KEY_TYPE, DSO_TYPE> mainCache =
                getMainCache();
        for(KEY_TYPE key : keys){
            result.add(mapper.mapToDTO(mainCache.get(key),
                    getStorageNodeName(key)));
        }
        return result;
    }

    /**
     *
     * Метод выборки не очень хороший. Точнее очень нехороший.
     * Будет время - лучше подумать как сделать качественнее
     * Например, строить дополнительный индекс по классу, например
     *
     * Class как ключ -> concurrent коллекция первичных ключей -> значения
     *
     * @return метод возвращает все коллекции из кэша
     */
    @SuppressWarnings("unchecked")
    protected List<DTO_TYPE> getAll(){

        IgniteCache<KEY_TYPE, Object> mainCache =
                ignite.getOrCreateCache(mainCacheName);

        Iterator<Cache.Entry<KEY_TYPE, Object>> iterator =
                mainCache.iterator();

        List<DTO_TYPE> result = new ArrayList<>();

        while(iterator.hasNext()){
            Cache.Entry<KEY_TYPE, Object> entry =
                    iterator.next();

            if (entry.getValue().getClass().equals(entityClass)){
                DSO_TYPE casted = (DSO_TYPE) entry.getValue();

                result.add(mapper.mapToDTO(casted,
                        getStorageNodeName(entry.getKey())));
            }

        }
        return result;
    }


    private IgniteCache<Object, List<KEY_TYPE>>
    getIgniteCacheByName(String cacheName){
        return ignite.getOrCreateCache(cacheName);
    }

    private IgniteCache<KEY_TYPE, DSO_TYPE> getMainCache(){
        return ignite.getOrCreateCache(AnnotationProcessor
                .getMainCacheNameForClass(entityClass));
    }


    private void removeIndicesForValue(DSO_TYPE value){
        Map<String, Object> indicesValues =
                getIndicesValues(processingInfo, value);

        for(Map.Entry<String, IndexProcessingInfo> index :
                processingInfo.entrySet()){
            IndexProcessingInfo info = index.getValue();

            if(info.hasGlobalIndexing()){
                // creating global indices
                IgniteCache<Object, List<KEY_TYPE>> indexCache =
                        getIgniteCacheByName(info.getGlobalIndexCacheName());

                List<KEY_TYPE> primaryKeys;
                Object secondaryKey =
                        indicesValues.get(index.getKey());

                if(indexCache.containsKey(secondaryKey)){
                    primaryKeys = indexCache.get(secondaryKey);
                    primaryKeys.remove(value.getKey());
                    indexCache.replace(secondaryKey, primaryKeys);
                }

            }

            if(info.hasLocalIndexing()){
                // creating local indices for value
                IgniteCache<Object, List<KEY_TYPE>> indexCache =
                        getIgniteCacheByName(info.getLocalIndexCacheName());

                long partitionId = value.key.getPartitionId();

                Object secondaryKey =
                        indicesValues.get(index.getKey());

                LocalIndex<Object, DSO_TYPE> localIndexKey =
                        new LocalIndex<>(partitionId,
                                secondaryKey,
                                entityClass);

                List<KEY_TYPE> primaryKeys;

                if(indexCache.containsKey(localIndexKey)){
                    primaryKeys = indexCache.get(localIndexKey);
                    primaryKeys.remove(value.getKey());
                    indexCache.put(localIndexKey, primaryKeys);
                }


            }

        }
    }


    private void createIndicesForValue(DSO_TYPE value){
        Map<String, Object> indicesValues =
                getIndicesValues(processingInfo, value);

        for(Map.Entry<String, IndexProcessingInfo> index :
                processingInfo.entrySet()){
            IndexProcessingInfo info = index.getValue();

            if(info.hasGlobalIndexing()){
                // creating global indices
                IgniteCache<Object, List<KEY_TYPE>> indexCache =
                        getIgniteCacheByName(info.getGlobalIndexCacheName());

                List<KEY_TYPE> primaryKeys;
                Object secondaryKey =
                        indicesValues.get(index.getKey());

                if(indexCache.containsKey(secondaryKey)){
                    primaryKeys = indexCache.get(secondaryKey);
                    primaryKeys.add(value.getKey());
                    indexCache.replace(secondaryKey, primaryKeys);
                } else {
                    primaryKeys = new ArrayList<>();
                    primaryKeys.add(value.getKey());
                    indexCache.put(secondaryKey, primaryKeys);
                }

            }

            if(info.hasLocalIndexing()){
                // creating local indices for value
                IgniteCache<Object, List<KEY_TYPE>> indexCache =
                        getIgniteCacheByName(info.getLocalIndexCacheName());

                long partitionId = value.key.getPartitionId();

                Object secondaryKey =
                        indicesValues.get(index.getKey());

                LocalIndex<Object, DSO_TYPE> localIndexKey =
                        new LocalIndex<>(partitionId,
                                secondaryKey,
                                entityClass);

                List<KEY_TYPE> primaryKeys;

                if(indexCache.containsKey(localIndexKey)){
                    primaryKeys = indexCache.get(localIndexKey);
                    primaryKeys.add(value.getKey());
                    indexCache.put(localIndexKey, primaryKeys);
                } else {
                    primaryKeys = new ArrayList<>();
                    primaryKeys.add(value.getKey());
                    indexCache.put(localIndexKey, primaryKeys);
                }


            }

        }
    }



    public String getMainCacheName() {
        return mainCacheName;
    }
}
