package com.sbt.entities.abstractAndInterfaces;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Vladimir Aseev on 31.03.2017.
 */
@Target(ElementType.ANNOTATION_TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface CompositeIndex {

    String name();
    String[] fields();
    IndexKind indexKind() default IndexKind.GLOBAL_ONLY;

}
