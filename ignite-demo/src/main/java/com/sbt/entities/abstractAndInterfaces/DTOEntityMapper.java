package com.sbt.entities.abstractAndInterfaces;

import com.sbt.daoInterfaces.NodeName;

/**
 * Created by Vladimir Aseev on 31.03.2017.
 */
public abstract class DTOEntityMapper<DSO_TYPE extends AbstractDataEntity<? extends KEY_TYPE>,
DTO_TYPE, KEY_TYPE> {

    public abstract DTO_TYPE mapToDTO(DSO_TYPE dsoObject, NodeName storageNodeName);

    public abstract DSO_TYPE mapToDSO(DTO_TYPE dtoObject, KEY_TYPE key);


}
