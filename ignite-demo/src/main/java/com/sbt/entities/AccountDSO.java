package com.sbt.entities;


import com.sbt.entities.abstractAndInterfaces.AbstractDataEntity;
import com.sbt.entities.abstractAndInterfaces.CacheNameSpecified;
import com.sbt.entities.abstractAndInterfaces.Index;
import com.sbt.entities.affinity.CollocatedWithClient;

/**
 * Created by Vladimir Aseev on 11.03.2017.
 */
@CacheNameSpecified(cacheName = "clientRelatedData")
public class AccountDSO extends AbstractDataEntity<CollocatedWithClient<Long>> {


    @Index
    private final String accountNumber;
    @Index
    private final long clientId;
    private final long funds;


    public AccountDSO(CollocatedWithClient<Long> key,
                      long clientId,
                      String accountNumber,
                      long funds) {
        super(key);
        this.accountNumber = accountNumber;
        this.clientId = clientId;
        this.funds = funds;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public long getFunds() {
        return funds;
    }

    public long getClientId() {
        return clientId;
    }

    public long getAccountId(){
        return key.getEntityId();
    }
}
