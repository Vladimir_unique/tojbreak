package com.sbt.entities;

import com.sbt.entities.abstractAndInterfaces.*;
import com.sbt.entities.affinity.PartitionIdResolver;
import com.sbt.entities.affinity.PartitioningKey;

/**
 * Created by Vladimir Aseev on 01.04.2017.
 */
@CompositeIndices( compositeIndices = {
    @CompositeIndex(name = "documentCredentials", fields = {
            "relatedDocumentName",
            "paymentDocumentPartNumber"
    })
})
@CacheNameSpecified(cacheName = "documentsParts")
public class PaymentDocumentPartDSO extends AbstractDataEntity<PartitioningKey>{

    @Index(indexKind = IndexKind.GLOBAL_AND_LOCAL)
    private final String relatedDocumentName;
    private final int paymentDocumentPartNumber;
    private final String documentPartData;

    public PaymentDocumentPartDSO(String relatedDocumentName,
                                  int paymentDocumentPartNumber,
                                  String documentPartData) {
        super(
                new PartitioningKey(PartitionIdResolver
                        .getPartitionNumberForDocumentPart(relatedDocumentName,
                                paymentDocumentPartNumber)));
        this.relatedDocumentName = relatedDocumentName;
        this.paymentDocumentPartNumber = paymentDocumentPartNumber;
        this.documentPartData = documentPartData;
    }

    public String getRelatedDocumentName() {
        return relatedDocumentName;
    }

    public int getPaymentDocumentPartNumber() {
        return paymentDocumentPartNumber;
    }

    public String getDocumentPartData() {
        return documentPartData;
    }
}
