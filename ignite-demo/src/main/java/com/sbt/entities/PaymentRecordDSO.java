package com.sbt.entities;

import com.sbt.dto.PaymentRecordProcessingStatus;
import com.sbt.entities.abstractAndInterfaces.*;
import com.sbt.entities.affinity.CollocatedWithClient;

/**
 * Created by Vladimir Aseev on 01.04.2017.
 */
@CompositeIndices(compositeIndices = {
        @CompositeIndex(name = "clientCredentials", fields = {
                "clientFirstName",
                "clientLastName",
                "clientMiddleName"
        }),
        @CompositeIndex(name = "documentNameAndAccountNumber", fields ={
                "paymentDocumentName",
                "paymentAccountNumber"
        })
})
@CacheNameSpecified(cacheName = "clientRelatedData")
public class PaymentRecordDSO extends AbstractDataEntity<CollocatedWithClient<Long>> {

    @Index(indexKind = IndexKind.GLOBAL_AND_LOCAL)
    private final String paymentDocumentName;

    @Index
    private final String paymentAccountNumber;
    private final String clientFirstName;
    private final String clientLastName;
    private final String clientMiddleName;
    private final long paymentAmount;
    private final PaymentRecordProcessingStatus status;

    public PaymentRecordDSO(CollocatedWithClient<Long> key,
                            String paymentDocumentName,
                            String paymentAccountNumber,
                            String clientFirstName,
                            String clientLastName,
                            String clientMiddleName,
                            long paymentAmount,
                            PaymentRecordProcessingStatus status) {
        super(key);
        this.paymentDocumentName = paymentDocumentName;
        this.paymentAccountNumber = paymentAccountNumber;
        this.clientFirstName = clientFirstName;
        this.clientLastName = clientLastName;
        this.clientMiddleName = clientMiddleName;
        this.paymentAmount = paymentAmount;
        this.status = status;
    }

    public String getPaymentDocumentName() {
        return paymentDocumentName;
    }

    public String getPaymentAccountNumber() {
        return paymentAccountNumber;
    }

    public String getClientFirstName() {
        return clientFirstName;
    }

    public String getClientLastName() {
        return clientLastName;
    }

    public String getClientMiddleName() {
        return clientMiddleName;
    }

    public long getPaymentAmount() {
        return paymentAmount;
    }

    public PaymentRecordProcessingStatus getStatus() {
        return status;
    }
}
