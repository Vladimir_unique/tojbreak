package com.sbt.entities.affinity;

import org.apache.ignite.cache.affinity.AffinityKeyMapped;

/**
 * Created by Vladimir Aseev on 02.04.2017.
 */
public class PartitioningKey {

    @AffinityKeyMapped
    private final long partitionId;

    public PartitioningKey(long partitionId) {
        this.partitionId = partitionId;
    }

    public long getPartitionId() {
        return partitionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PartitioningKey that = (PartitioningKey) o;

        return partitionId == that.partitionId;
    }

    @Override
    public int hashCode() {
        return (int) (partitionId ^ (partitionId >>> 32));
    }
}
