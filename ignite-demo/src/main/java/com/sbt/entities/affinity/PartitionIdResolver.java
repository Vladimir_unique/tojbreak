package com.sbt.entities.affinity;

/**
 * Created by Vladimir Aseev on 02.04.2017.
 */
public class PartitionIdResolver {

    private static final long PARTITIONS_COUNT = 1023L;

    private static final long BITS_COUNT;

    static {
        long tmp = 65L;
        boolean bitFound = false;
        while((tmp > 0L) && (!bitFound)){
            tmp--;
            if(((1L << (tmp-1)) & PARTITIONS_COUNT) != 0L){
                bitFound = true;
            }
        }
        BITS_COUNT = tmp;
    }

    private static final long MASK_FOR_HASHCODE;

    static {
        long tmp = 0L;
        for(long l = 1L; l <= BITS_COUNT; l++){
            tmp = tmp | (1L << (l - 1L));
        }
        MASK_FOR_HASHCODE = tmp;
    }

    public static long getPartitionNumberForDocumentPart(String documentName,
                                                         int documentPartNumber){
        return (documentName.hashCode() ^ documentPartNumber)
                & PARTITIONS_COUNT;
    }

    public static long getPartitionNumberForDocument(String documentName){
        return documentName.hashCode() & PARTITIONS_COUNT;
    }


    public static long getPartitonNumberForClientId(long clientId){
        return PARTITIONS_COUNT & clientId;
    }

    public static long getPartitionsCount() {
        return PARTITIONS_COUNT;
    }

    public static long getMaskBitsCount(){
        return BITS_COUNT;
    }

    public static long getMaskForHashcode(){
        return MASK_FOR_HASHCODE;
    }
}
