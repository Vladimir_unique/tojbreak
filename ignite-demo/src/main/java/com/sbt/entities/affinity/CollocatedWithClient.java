package com.sbt.entities.affinity;

import com.sbt.entities.abstractAndInterfaces.Index;

/**
 * Created by Vladimir Aseev on 01.04.2017.
 */
public class CollocatedWithClient<KEY_TYPE> extends PartitioningKey {

    @Index
    private final long clientId;
    private final KEY_TYPE entityId;

    public CollocatedWithClient(long clientId, KEY_TYPE entityId) {
        super(PartitionIdResolver
                .getPartitonNumberForClientId(clientId));
        this.clientId = clientId;
        this.entityId = entityId;
    }


    public long getClientId() {
        return clientId;
    }

    public KEY_TYPE getEntityId() {
        return entityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CollocatedWithClient<?> that = (CollocatedWithClient<?>) o;

        if (clientId != that.clientId) return false;
        return entityId.equals(that.entityId);
    }

    @Override
    public int hashCode() {
        int result = (int) (clientId ^ (clientId >>> 32));
        result = 31 * result + entityId.hashCode();
        return result;
    }
}
