package com.sbt.entities.affinity;

/**
 * Created by Vladimir Aseev on 04.04.2017.
 */
public class PaymentDocumentsDataKey<KEY_TYPE> extends PartitioningKey {


    private final KEY_TYPE key;

    public PaymentDocumentsDataKey(KEY_TYPE key,
                                   long partitionId) {
        super(partitionId);
        this.key = key;
    }

    public KEY_TYPE getKey() {
        return key;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PaymentDocumentsDataKey<?> that = (PaymentDocumentsDataKey<?>) o;

        return key.equals(that.key);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + key.hashCode();
        return result;
    }
}
