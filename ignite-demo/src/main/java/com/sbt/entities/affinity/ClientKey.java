package com.sbt.entities.affinity;

/**
 * Created by Vladimir Aseev on 02.04.2017.
 */
public class ClientKey extends PartitioningKey {

    private final long clientId;

    public ClientKey(long clientId) {
        super(                PartitionIdResolver
                .getPartitonNumberForClientId(clientId));
        this.clientId = clientId;
    }

    public long getClientId() {
        return clientId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ClientKey clientKey = (ClientKey) o;

        return clientId == clientKey.clientId;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) (clientId ^ (clientId >>> 32));
        return result;
    }
}
