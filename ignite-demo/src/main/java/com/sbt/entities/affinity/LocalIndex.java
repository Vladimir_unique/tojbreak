package com.sbt.entities.affinity;

import com.sbt.entities.abstractAndInterfaces.AbstractDataEntity;
import org.apache.ignite.cache.affinity.AffinityKeyMapped;

/**
 * Created by Vladimir Aseev on 03.04.2017.
 */
public class LocalIndex<SEARCHING_VALUE_TYPE, DSO extends AbstractDataEntity> {

    @AffinityKeyMapped
    private final long partitionId;
    private final SEARCHING_VALUE_TYPE searchingValue;
    private final String storageEntityClassName;

    public LocalIndex(long partitionId,
                      SEARCHING_VALUE_TYPE searchingValue,
                      Class<DSO> storageEntityClass) {
        this.partitionId = partitionId;
        this.searchingValue = searchingValue;
        this.storageEntityClassName = storageEntityClass
                .getCanonicalName();
    }


    public long getPartitionId() {
        return partitionId;
    }

    public SEARCHING_VALUE_TYPE getSearchingValue() {
        return searchingValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LocalIndex<?, ?> that = (LocalIndex<?, ?>) o;

        if (partitionId != that.partitionId) return false;
        if (!searchingValue.equals(that.searchingValue)) return false;
        return storageEntityClassName.equals(that.storageEntityClassName);
    }

    @Override
    public int hashCode() {
        int result = (int) (partitionId ^ (partitionId >>> 32));
        result = 31 * result + searchingValue.hashCode();
        result = 31 * result + storageEntityClassName.hashCode();
        return result;
    }
}
