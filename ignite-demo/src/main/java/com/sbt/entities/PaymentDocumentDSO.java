package com.sbt.entities;

import com.sbt.dto.PaymentDocumentStatus;
import com.sbt.entities.abstractAndInterfaces.AbstractDataEntity;
import com.sbt.entities.abstractAndInterfaces.CacheNameSpecified;
import com.sbt.entities.abstractAndInterfaces.Index;
import com.sbt.entities.affinity.PartitionIdResolver;
import com.sbt.entities.affinity.PaymentDocumentsDataKey;

/**
 * Created by Vladimir Aseev on 01.04.2017.
 */
@CacheNameSpecified(cacheName = "documentsParts")
public class PaymentDocumentDSO extends
        AbstractDataEntity<PaymentDocumentsDataKey<String>> {

    @Index
    private final String documentName;
    @Index
    private final PaymentDocumentStatus status;

    private final int documentPartsCount;

    public PaymentDocumentDSO(String documentName,
                              PaymentDocumentStatus status,
                              int documentPartsCount) {
        super(new PaymentDocumentsDataKey<>(documentName,
                PartitionIdResolver
                .getPartitionNumberForDocument(documentName)));
        this.documentName = documentName;
        this.status = status;
        this.documentPartsCount = documentPartsCount;
    }

    public String getDocumentName() {
        return documentName;
    }

    public PaymentDocumentStatus getStatus() {
        return status;
    }

    public int getDocumentPartsCount() {
        return documentPartsCount;
    }
}
