package com.sbt.demo;

import org.apache.ignite.*;
import org.apache.ignite.cache.affinity.Affinity;
import org.apache.ignite.cluster.ClusterGroup;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.CollectionConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.configuration.NearCacheConfiguration;
import org.apache.ignite.lang.IgniteProductVersion;
import org.apache.ignite.plugin.IgnitePlugin;
import org.apache.ignite.plugin.PluginNotFoundException;
import org.jetbrains.annotations.Nullable;

import javax.cache.CacheException;
import java.util.Collection;
import java.util.concurrent.ExecutorService;

/**
 * Created by Vladimir Aseev on 01.04.2017.
 */
public class IgniteProvider implements Ignite {


    private final IgniteConfiguration cfg;
    private final Ignite ignite;

    public IgniteProvider(IgniteConfiguration cfg) {
        this.cfg = cfg;
        this.ignite = Ignition.getOrStart(cfg);
    }

    @Override
    public String name() {
        return ignite.name();
    }

    @Override
    public IgniteLogger log() {
        return ignite.log();
    }

    @Override
    public IgniteConfiguration configuration() {
        return ignite.configuration();
    }

    @Override
    public IgniteCluster cluster() {
        return ignite.cluster();
    }

    @Override
    public IgniteCompute compute() {
        return ignite.compute();
    }

    @Override
    public IgniteCompute compute(ClusterGroup grp) {
        return ignite.compute(grp);
    }

    @Override
    public IgniteMessaging message() {
        return ignite.message();
    }

    @Override
    public IgniteMessaging message(ClusterGroup grp) {
        return ignite.message(grp);
    }

    @Override
    public IgniteEvents events() {
        return ignite.events();
    }

    @Override
    public IgniteEvents events(ClusterGroup grp) {
        return ignite.events(grp);
    }

    @Override
    public IgniteServices services() {
        return ignite.services();
    }

    @Override
    public IgniteServices services(ClusterGroup grp) {
        return ignite.services(grp);
    }

    @Override
    public ExecutorService executorService() {
        return ignite.executorService();
    }

    @Override
    public ExecutorService executorService(ClusterGroup grp) {
        return ignite.executorService(grp);
    }

    @Override
    public IgniteProductVersion version() {
        return ignite.version();
    }

    @Override
    public IgniteScheduler scheduler() {
        return ignite.scheduler();
    }

    @Override
    public <K, V> IgniteCache<K, V> createCache(CacheConfiguration<K, V> cacheCfg) throws CacheException {
        return ignite.createCache(cacheCfg);
    }

    @Override
    public Collection<IgniteCache> createCaches(Collection<CacheConfiguration> cacheCfgs) throws CacheException {
        return createCaches(cacheCfgs);
    }

    @Override
    public <K, V> IgniteCache<K, V> createCache(String cacheName) throws CacheException {
        return ignite.createCache(cacheName);
    }

    @Override
    public <K, V> IgniteCache<K, V> getOrCreateCache(CacheConfiguration<K, V> cacheCfg) throws CacheException {
        return ignite.getOrCreateCache(cacheCfg);
    }

    @Override
    public <K, V> IgniteCache<K, V> getOrCreateCache(String cacheName) throws CacheException {
        return ignite.getOrCreateCache(cacheName);
    }

    @Override
    public Collection<IgniteCache> getOrCreateCaches(Collection<CacheConfiguration> cacheCfgs) throws CacheException {
        return ignite.getOrCreateCaches(cacheCfgs);
    }

    @Override
    public <K, V> void addCacheConfiguration(CacheConfiguration<K, V> cacheCfg) throws CacheException {
        ignite.addCacheConfiguration(cacheCfg);
    }

    @Override
    public <K, V> IgniteCache<K, V> createCache(CacheConfiguration<K, V> cacheCfg, NearCacheConfiguration<K, V> nearCfg) throws CacheException {
        return ignite.createCache(cacheCfg, nearCfg);
    }

    @Override
    public <K, V> IgniteCache<K, V> getOrCreateCache(CacheConfiguration<K, V> cacheCfg, NearCacheConfiguration<K, V> nearCfg) throws CacheException {
        return ignite.getOrCreateCache(cacheCfg, nearCfg);
    }

    @Override
    public <K, V> IgniteCache<K, V> createNearCache(@Nullable String cacheName, NearCacheConfiguration<K, V> nearCfg) throws CacheException {
        return ignite.createNearCache(cacheName, nearCfg);
    }

    @Override
    public <K, V> IgniteCache<K, V> getOrCreateNearCache(@Nullable String cacheName, NearCacheConfiguration<K, V> nearCfg) throws CacheException {
        return ignite.getOrCreateNearCache(cacheName, nearCfg);
    }

    @Override
    public void destroyCache(String cacheName) throws CacheException {
        ignite.destroyCache(cacheName);
    }

    @Override
    public void destroyCaches(Collection<String> cacheNames) throws CacheException {
        ignite.destroyCaches(cacheNames);
    }

    @Override
    public <K, V> IgniteCache<K, V> cache(@Nullable String name) throws CacheException {
        return ignite.cache(name);
    }

    @Override
    public Collection<String> cacheNames() {
        return ignite.cacheNames();
    }

    @Override
    public IgniteTransactions transactions() {
        return ignite.transactions();
    }

    @Override
    public <K, V> IgniteDataStreamer<K, V> dataStreamer(@Nullable String cacheName) throws IllegalStateException {
        return ignite.dataStreamer(cacheName);
    }

    @Override
    public IgniteFileSystem fileSystem(String name) throws IllegalArgumentException {
        return ignite.fileSystem(name);
    }

    @Override
    public Collection<IgniteFileSystem> fileSystems() {
        return ignite.fileSystems();
    }

    @Override
    public IgniteAtomicSequence atomicSequence(String name, long initVal, boolean create) throws IgniteException {
        return ignite.atomicSequence(name, initVal, create);
    }

    @Override
    public IgniteAtomicLong atomicLong(String name, long initVal, boolean create) throws IgniteException {
        return ignite.atomicLong(name, initVal, create);
    }

    @Override
    public <T> IgniteAtomicReference<T> atomicReference(String name, @Nullable T initVal, boolean create) throws IgniteException {
        return ignite.atomicReference(name, initVal, create);
    }

    @Override
    public <T, S> IgniteAtomicStamped<T, S> atomicStamped(String name, @Nullable T initVal, @Nullable S initStamp, boolean create) throws IgniteException {
        return ignite.atomicStamped(name, initVal, initStamp, create);
    }

    @Override
    public IgniteCountDownLatch countDownLatch(String name, int cnt, boolean autoDel, boolean create) throws IgniteException {
        return ignite.countDownLatch(name, cnt, autoDel, create);
    }

    @Override
    public IgniteSemaphore semaphore(String name, int cnt, boolean failoverSafe, boolean create) throws IgniteException {
        return ignite.semaphore(name, cnt, failoverSafe, create);
    }

    @Override
    public IgniteLock reentrantLock(String name, boolean failoverSafe, boolean fair, boolean create) throws IgniteException {
        return ignite.reentrantLock(name, failoverSafe, fair, create);
    }

    @Override
    public <T> IgniteQueue<T> queue(String name, int cap, @Nullable CollectionConfiguration cfg) throws IgniteException {
        return ignite.queue(name, cap, cfg);
    }

    @Override
    public <T> IgniteSet<T> set(String name, @Nullable CollectionConfiguration cfg) throws IgniteException {
        return ignite.set(name, cfg);
    }

    @Override
    public <T extends IgnitePlugin> T plugin(String name) throws PluginNotFoundException {
        return ignite.plugin(name);
    }

    @Override
    public IgniteBinary binary() {
        return ignite.binary();
    }

    @Override
    public void close() throws IgniteException {
        ignite.close();
    }

    @Override
    public <K> Affinity<K> affinity(String cacheName) {
        return ignite.affinity(cacheName);
    }

}

