package com.sbt.demo;

import com.sbt.common.SuperclassForDemo;
import com.sbt.dao.IgniteDAOFactory;
import com.sbt.daoInterfaces.DAOFactory;
import com.sbt.daoInterfaces.PaymentRecordDAO;
import com.sbt.dto.MethodResult;
import com.sbt.dto.PaymentRecordDTO;
import com.sbt.dto.PaymentRecordProcessingStatus;
import org.apache.ignite.Ignite;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Vladimir Aseev on 07.03.2017.
 *
 * Это основной серверный процесс.
 *
 * Он немного странный для серверного процесса,
 * прямо изобилует сложной логикой, как вы видите =)
 *
 * но тем не менее - это он. вся мозговитая
 * бизнес-логика грузится на него динамически
 *
 */
public class DemoServer extends SuperclassForDemo {

    public static void main(String[] args) {

        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("server-context.xml");

        Ignite ignite = context.getBean(Ignite.class);


        waitUserInputAndBlock();
        ignite.close();
    }

}
