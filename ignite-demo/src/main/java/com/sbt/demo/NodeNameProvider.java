package com.sbt.demo;

import com.sbt.daoInterfaces.NodeName;

import java.util.Scanner;

/**
 * Created by Vladimir Aseev on 01.04.2017.
 */
public class NodeNameProvider {


    private final NodeName nodeName;
    private final String nodeNamePropertyName;


    public NodeNameProvider(){
        nodeNamePropertyName = NodeName.NODE_COLOR_ATTRIBUTE_NAME;
        nodeName = readNodeName();
    }


    private NodeName readNodeName(){
        Scanner sc = new Scanner(System.in);

        try{
            String candidate = System.getProperty(NodeName.NODE_COLOR_ATTRIBUTE_NAME);
            return NodeName.valueOf(candidate.toUpperCase());
        } catch (Exception e){
            System.err.println("Warning: Node name attribute is not defined as JVM property.");
        }

        while(true){
            try{
                System.err.println("Input node name...");
                System.err.println();
                    String candidate =
                            sc.nextLine();
                    candidate =
                            candidate.toUpperCase();
                    try{
                        NodeName name = NodeName.valueOf(candidate);
                        System.err.println("Parsed nodeName: " + name.name());
                        return name;
                    } catch (Exception e){
                        System.err.println("Incorrect input! Type \"RED\", \"GREEN\" or \"BLUE\" ");
                    }
            } catch (Exception e){
                e.printStackTrace();
                break;
            }
        }
        return NodeName.UNKNOWN;

    }

    public NodeName getNodeName() {
        return nodeName;
    }

    public String getNodeNamePropertyName() {
        return nodeNamePropertyName;
    }
}
