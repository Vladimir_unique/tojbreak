package com.sbt.dto;

import com.sbt.daoInterfaces.NodeName;

/**
 * Created by Vladimir Aseev on 23.03.2017.
 */
public class AccountDTO {

    private final long id;
    private final long clientId;
    private final String accountNumber;
    private long balance;
    private final NodeName storageNodeName;


    public AccountDTO(long id,
                      long clientId,
                      String accountNumber,
                      long balance,
                      NodeName storageNodeName) {
        this.id = id;
        this.clientId = clientId;
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.storageNodeName = storageNodeName;
    }

    public long getId() {
        return id;
    }

    public long getClientId() {
        return clientId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public long getBalance() {
        return balance;
    }

    public NodeName getStorageNodeName() {
        return storageNodeName;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }
}
