package com.sbt.dto;

/**
 * Created by Vladimir Aseev on 23.03.2017.
 */
public class RawPaymentRecordDTO {

    private final String accountNumber;
    private final String clientFirstName;
    private final String clientLastName;
    private final String clientMiddleName;
    private final double paymentAmount;
    private final PaymentRecordProcessingStatus status;


    public RawPaymentRecordDTO(String accountNumber,
                               String clientFirstName,
                               String clientLastName,
                               String clientMiddleName,
                               double paymentAmount,
                               PaymentRecordProcessingStatus status) {
        this.accountNumber = accountNumber;
        this.clientFirstName = clientFirstName;
        this.clientLastName = clientLastName;
        this.clientMiddleName = clientMiddleName;
        this.paymentAmount = paymentAmount;
        this.status = status;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getClientFirstName() {
        return clientFirstName;
    }

    public String getClientLastName() {
        return clientLastName;
    }

    public String getClientMiddleName() {
        return clientMiddleName;
    }

    public double getPaymentAmount() {
        return paymentAmount;
    }

    public PaymentRecordProcessingStatus getStatus() {
        return status;
    }
}
