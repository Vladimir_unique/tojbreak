package com.sbt.dto;

/**
 * Created by Vladimir Aseev on 23.03.2017.
 */
public final class MethodResult<T> {


    private final MethodResultStatus resultStatus;
    private final String description;
    private final T result;

    private MethodResult(T result){
        this.resultStatus = MethodResultStatus.SUCCESS;
        this.result = result;
        this.description = "Ok.";
    }

    private MethodResult(String description, MethodResultStatus status){
        this.description = description;
        this.resultStatus = status;
        this.result = null;
    }


    public static <V> MethodResult<V> ok(V result){
        return new MethodResult<>(result);
    }

    public static <V> MethodResult<V> onError(String description){
        return new MethodResult<>(description, MethodResultStatus.ERROR);
    }

    public static <V> MethodResult<V> onNotFound(String description){
        return new MethodResult<>(description, MethodResultStatus.NOT_FOUND);
    }

    public MethodResultStatus getResultStatus() {
        return resultStatus;
    }

    public String getDescription() {
        return description;
    }

    public T getResult() {
        return result;
    }
}
