package com.sbt.dto;

import com.sbt.daoInterfaces.NodeName;

/**
 * Created by Vladimir Aseev on 23.03.2017.
 */
public class PaymentRecordDTO {



    private final long id;
    private final String paymentDocumentName;
    private final String accountNumber;
    private final String clientFirstName;
    private final String clientLastName;
    private final String clientMiddleName;
    private final long paymentAmount;
    private final long clientId;
    private final NodeName storageNodeName;
    private final PaymentRecordProcessingStatus processingStatus;


    public PaymentRecordDTO(long id,
                            String paymentDocumentName,
                            long clientId,
                            String accountNumber,
                            String clientFirstName,
                            String clientLastName,
                            String clientMiddleName,
                            long paymentAmount,
                            NodeName storageNodeName,
                            PaymentRecordProcessingStatus processingStatus){
        this.id = id;
        this.paymentDocumentName = paymentDocumentName;
        this.accountNumber = accountNumber;
        this.clientFirstName = clientFirstName;
        this.clientLastName = clientLastName;
        this.clientMiddleName = clientMiddleName;
        this.paymentAmount = paymentAmount;
        this.storageNodeName = storageNodeName;
        this.clientId = clientId;
        this.processingStatus = processingStatus;
    }

    public long getId() {
        return id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getClientFirstName() {
        return clientFirstName;
    }

    public String getClientLastName() {
        return clientLastName;
    }

    public String getClientMiddleName() {
        return clientMiddleName;
    }

    public long getPaymentAmount() {
        return paymentAmount;
    }

    public String getPaymentDocumentName() {
        return paymentDocumentName;
    }

    public NodeName getStorageNodeName() {
        return storageNodeName;
    }

    public long getClientId() {
        return clientId;
    }

    public PaymentRecordProcessingStatus getProcessingStatus() {
        return processingStatus;
    }
}
