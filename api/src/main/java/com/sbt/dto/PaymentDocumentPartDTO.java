package com.sbt.dto;

import com.sbt.daoInterfaces.NodeName;

/**
 * Created by Vladimir Aseev on 30.03.2017.
 */
public class PaymentDocumentPartDTO {

    private final long partitionId;
    private final String paymentDocumentName;
    private final int paymentDocumentPartNumber;
    private final String documentPartData;
    private final NodeName storageNodeName;


    public PaymentDocumentPartDTO(long partitionId, String paymentDocumentName,
                                  int paymentDocumentPartNumber,
                                  String documentPartData,
                                  NodeName storageNodeName) {
        this.partitionId = partitionId;
        this.paymentDocumentName = paymentDocumentName;
        this.paymentDocumentPartNumber = paymentDocumentPartNumber;
        this.documentPartData = documentPartData;
        this.storageNodeName = storageNodeName;
    }

    public String getPaymentDocumentName() {
        return paymentDocumentName;
    }

    public int getPaymentDocumentPartNumber() {
        return paymentDocumentPartNumber;
    }

    public String getDocumentPartData() {
        return documentPartData;
    }

    public NodeName getStorageNodeName() {
        return storageNodeName;
    }

    public long getPartitionId() {
        return partitionId;
    }
}
