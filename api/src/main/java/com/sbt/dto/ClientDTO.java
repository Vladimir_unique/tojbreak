package com.sbt.dto;

import com.sbt.daoInterfaces.NodeName;

/**
 * Created by Vladimir Aseev on 23.03.2017.
 */
public class ClientDTO {

    private final long id;
    private final String firstName;
    private final String lastName;
    private final String middleName;
    private final NodeName storageNodeName;


    public ClientDTO(long id,
                     String firstName,
                     String lastName,
                     String middleName,
                     NodeName storageNodeName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.storageNodeName = storageNodeName;
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public NodeName getStorageNodeName() {
        return storageNodeName;
    }
}
