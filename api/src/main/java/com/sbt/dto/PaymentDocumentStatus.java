package com.sbt.dto;

/**
 * Created by Vladimir Aseev on 23.03.2017.
 */
public enum PaymentDocumentStatus {

    NEW("В процессе сохранения"),
    SAVED("Сохранён"),
    IN_PROGRESS("В обработке"),
    PARSED("Произведён парсинг"),
    VALIDATED("Произведена валидация"),
    PAYMENTS_FINISHED("Выплаты произведены");


    String textForGui;

    PaymentDocumentStatus(String textForGui){
        this.textForGui = textForGui;
    }

    public String getTextForGui() {
        return textForGui;
    }


}
