package com.sbt.dto;

import com.sbt.daoInterfaces.NodeName;

/**
 * Created by Vladimir Aseev on 23.03.2017.
 */
public class PaymentDocumentDTO {

    private final String documentName;
    private final PaymentDocumentStatus status;
    private final NodeName storageNodeName;
    private final int documentPartsCount;


    public PaymentDocumentDTO(String documentName,
                              PaymentDocumentStatus status,
                              NodeName storageNodeName,
                              int documentPartsCount) {
        this.documentName = documentName;
        this.status = status;
        this.storageNodeName = storageNodeName;
        this.documentPartsCount = documentPartsCount;
    }

    public String getDocumentName() {
        return documentName;
    }

    public PaymentDocumentStatus getStatus() {
        return status;
    }

    @Override
    public int hashCode() {
        return documentName.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof PaymentDocumentDTO){
            return documentName
                    .equals(((PaymentDocumentDTO) obj)
                            .getDocumentName());
        } else {
            return false;
        }
    }

    public NodeName getStorageNodeName() {
        return storageNodeName;
    }

    public int getDocumentPartsCount() {
        return documentPartsCount;
    }
}
