package com.sbt.dto;

/**
 * Created by Vladimir Aseev on 23.03.2017.
 */
public enum PaymentRecordProcessingStatus {

    IN_PROCESSING,
    PARSED,
    VALIDATED_INVALID,
    VALIDATED_OK,
    PAYMENT_FINISHED,
    ERROR;

}
