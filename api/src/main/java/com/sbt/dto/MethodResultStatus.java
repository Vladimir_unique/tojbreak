package com.sbt.dto;

/**
 * Created by Vladimir Aseev on 23.03.2017.
 */
public enum MethodResultStatus {

    SUCCESS,
    NOT_FOUND,
    ERROR;

}
