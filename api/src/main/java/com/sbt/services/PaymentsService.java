package com.sbt.services;

import com.sbt.dto.MethodResult;
import com.sbt.dto.PaymentDocumentStatus;

/**
 * Created by Vladimir Aseev on 23.03.2017.
 */
public interface PaymentsService {


    MethodResult<PaymentDocumentStatus> saveNewPaymentsDocument(String paymentsDocumentName,
                                                                String paymentsDocumentBinary);

    MethodResult<PaymentDocumentStatus> parsePaymentsDocument(String paymentsDocumentName);

    MethodResult<PaymentDocumentStatus> validatePaymentsDocument(String paymentsDocumentName);

    MethodResult<PaymentDocumentStatus> startPaymentProcessForDocument(String paymentsDocumentName);



}
