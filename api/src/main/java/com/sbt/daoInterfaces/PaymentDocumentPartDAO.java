package com.sbt.daoInterfaces;

import com.sbt.dto.MethodResult;
import com.sbt.dto.PaymentDocumentPartDTO;

import java.util.List;

/**
 * Created by Vladimir Aseev on 30.03.2017.
 */
public interface PaymentDocumentPartDAO {


    MethodResult<PaymentDocumentPartDTO> createPaymentDocumentPart(String paymentDocumentName,
                                                                   int paymentDocumentPartNumber,
                                                                   String paymentDocumentPartData,
                                                                   boolean openNewTransaction);

    MethodResult<PaymentDocumentPartDTO> getPaymentDocumentPartByCredentials(String paymentDocumentName,
                                                                             int paymentDocumentPartNumber,
                                                                             boolean openNewTransaction);

    MethodResult<List<PaymentDocumentPartDTO>> getPaymentDocumentParts(String paymentDocumentName,
                                                                       boolean openNewTransaction);


    MethodResult<List<PaymentDocumentPartDTO>> getLocalPaymentDocumentParts(String documentName);

}
