package com.sbt.daoInterfaces;

import com.sbt.dto.AccountDTO;
import com.sbt.dto.MethodResult;

import java.util.List;

/**
 * Created by Vladimir Aseev on 23.03.2017.
 */
public interface AccountDAO {

    MethodResult<AccountDTO> createAccount(String accountNumber,
                                           long clientId,
                                           boolean openNewTransaction);

    MethodResult<List<AccountDTO>> getAccountsByClientId(long clientId,
                                                         boolean openNewTransaction);

    MethodResult<AccountDTO> getAccountByNumber(String accountNumber,
                                                boolean openNewTransaction);

    MethodResult<List<AccountDTO>> getAll(boolean openNewTransaction);

    MethodResult<AccountDTO> income(String accountNumber,
                                    long amount,
                                    boolean openNewTransaction);


}
