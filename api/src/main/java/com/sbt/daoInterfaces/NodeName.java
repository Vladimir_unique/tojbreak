package com.sbt.daoInterfaces;

/**
 * Created by Vladimir Aseev on 01.04.2017.
 */
public enum NodeName {

    RED,
    GREEN,
    BLUE,
    CLIENT,
    UNKNOWN,
    DEBUG;


    public static final String NODE_COLOR_ATTRIBUTE_NAME =
            "nodeColor";


}
