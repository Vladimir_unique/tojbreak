package com.sbt.daoInterfaces;

import com.sbt.dto.MethodResult;
import com.sbt.dto.PaymentRecordDTO;
import com.sbt.dto.PaymentRecordProcessingStatus;

import java.util.List;

/**
 * Created by Vladimir Aseev on 23.03.2017.
 */
public interface PaymentRecordDAO {

    MethodResult<PaymentRecordDTO> createRecord(String paymentDocumentName,
                                                long clientId,
                                                String accountNumber,
                                                String clientFirstName,
                                                String clientLastName,
                                                String clientMiddleName,
                                                long paymentAmount,
                                                boolean openNewTransaction);

    MethodResult<List<PaymentRecordDTO>> getByPaymentDocumentName(String paymentDocumentName,
                                                                  boolean openNewTransaction);

    MethodResult<PaymentRecordDTO> updateProcessingStatus(String paymentDocumentName,
                                                          String accountNumber,
                                                          PaymentRecordProcessingStatus newStatus,
                                                          boolean openNewTransaction);

    MethodResult<List<PaymentRecordDTO>> getAll(boolean openNewTransaction);

    MethodResult<List<PaymentRecordDTO>>
    getLocalPaymentRecords(String paymentDocumentName,
                           boolean openNewTransaction);


}
