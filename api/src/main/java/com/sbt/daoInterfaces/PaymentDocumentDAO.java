package com.sbt.daoInterfaces;

import com.sbt.dto.MethodResult;
import com.sbt.dto.PaymentDocumentDTO;
import com.sbt.dto.PaymentDocumentStatus;

import java.util.List;

/**
 * Created by Vladimir Aseev on 23.03.2017.
 */
public interface PaymentDocumentDAO {

    MethodResult<PaymentDocumentDTO> createDocument(String documentName,
                                                    int documentPartsCount,
                                                    boolean openNewTransaction);

    MethodResult<PaymentDocumentDTO> getByName(String documentName,
                                               boolean openNewTransaction);

    MethodResult<List<PaymentDocumentDTO>> getByStatus(PaymentDocumentStatus status,
                                                       boolean openNewTransaction);

    MethodResult<PaymentDocumentDTO> updateStatus(String documentName,
                                                  PaymentDocumentStatus newStatus,
                                                  boolean openNewTransaction);

    MethodResult<List<PaymentDocumentDTO>> getAll(boolean openNewTransaction);

}
