package com.sbt.daoInterfaces;

import com.sbt.dto.ClientDTO;
import com.sbt.dto.MethodResult;

import java.util.List;

/**
 * Created by Vladimir Aseev on 23.03.2017.
 */
public interface ClientDAO {


    MethodResult<ClientDTO> createClient(String firstName,
                                         String lastName,
                                         String middleName,
                                         boolean openNewTransaction);

    MethodResult<List<ClientDTO>> getClientByCredentials(String firstName,
                                                          String lastName,
                                                          String middleName,
                                                          boolean openNewTransaction);

    MethodResult<ClientDTO> getClientById(long clientId);

    MethodResult<List<ClientDTO>> getAll(boolean openNewTransaction);

}
