package com.sbt.daoInterfaces;

/**
 * Created by Vladimir Aseev on 23.03.2017.
 */
public interface DAOFactory {

    AccountDAO getAccountDAO();

    ClientDAO getClientDAO();

    PaymentDocumentDAO getPaymentDocumentDAO();

    PaymentRecordDAO getPaymentRecordDAO();

    PaymentDocumentPartDAO getPaymentDocumentPartDAO();

}
