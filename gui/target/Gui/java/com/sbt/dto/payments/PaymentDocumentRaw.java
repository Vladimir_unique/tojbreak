package com.sbt.dto.payments;

/**
 * Created by Vladimir Aseev on 25.03.2017.
 */
public class PaymentDocumentRaw {


    private final String documentName;
    private final String rawDocumentData;

    public PaymentDocumentRaw(String documentName, String rawDocumentData){
        this.documentName = documentName;
        this.rawDocumentData = rawDocumentData;
    }


    public String getDocumentName() {
        return documentName;
    }

    public String getRawDocumentData() {
        return rawDocumentData;
    }
}
