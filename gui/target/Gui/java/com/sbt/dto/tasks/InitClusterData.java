package com.sbt.dto.tasks;


import com.sbt.dto.tasks.abstracts.GuiTask;
import com.sbt.dto.tasks.abstracts.TaskKind;
import com.sbt.servlets.common.JSPPageName;
import com.sbt.servlets.common.ServletName;

/**
 * Created by Vladimir Aseev on 22.03.2017.
 */
public class InitClusterData extends GuiTask {

    public InitClusterData() {
        super(TaskKind.INIT_CLUSTER_DATA,
                "Инициализация",
                "Инициализация клиентских данных кластера",
                ServletName.INIT_CLUSTER_DATA.getServletAddress());
    }
}
