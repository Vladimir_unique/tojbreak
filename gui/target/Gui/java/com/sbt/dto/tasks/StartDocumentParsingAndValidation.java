package com.sbt.dto.tasks;

import com.sbt.dto.tasks.abstracts.GuiTask;
import com.sbt.dto.tasks.abstracts.TaskKind;
import com.sbt.servlets.common.JSPPageName;
import com.sbt.servlets.common.ServletName;

/**
 * Created by Vladimir Aseev on 22.03.2017.
 */
public class StartDocumentParsingAndValidation extends GuiTask {

    public StartDocumentParsingAndValidation() {
        super(TaskKind.START_DOCUMENT_PARSING_AND_VALIDATION,
                "Проверить документ",
                "Провести парсинг и валидацию документа",
                ServletName.START_DOCUMENT_PARSING_AND_VALIDATION.getServletAddress());
    }
}
