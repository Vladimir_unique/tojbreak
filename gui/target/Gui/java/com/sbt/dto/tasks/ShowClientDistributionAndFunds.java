package com.sbt.dto.tasks;

import com.sbt.dto.tasks.abstracts.GuiTask;
import com.sbt.dto.tasks.abstracts.TaskKind;
import com.sbt.servlets.common.ServletName;

/**
 * Created by Vladimir Aseev on 22.03.2017.
 */
public class ShowClientDistributionAndFunds extends GuiTask {



    public ShowClientDistributionAndFunds() {
        super(TaskKind.SHOW_CLIENTS_DISTRIBUTION_AND_FUNDS,
                "Информация о клиентах",
                "Показать информацию о клиентах и счетах",
                ServletName.SHOW_CLIENT_DISTRIBUTION.getServletAddress());
    }
}
