package com.sbt.dto.clientData;

/**
 * Created by Vladimir Aseev on 23.03.2017.
 */
public class ClientDataObject {

    private final long id;
    private final String clientNode;
    private final String accountNode;
    private final String firstName;
    private final String lastName;
    private final String middleName;
    private final String accountNumber;
    private final String balance;

    public ClientDataObject(long id,
                            String clientNode,
                            String accountNode,
                            String firstName,
                            String lastName,
                            String middleName,
                            String accountNumber,
                            String balance) {
        this.id = id;
        this.clientNode = clientNode;
        this.accountNode = accountNode;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.accountNumber = accountNumber;
        this.balance = balance;
    }

    public long getId() {
        return id;
    }

    public String getClientNode() {
        return clientNode;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getBalance() {
        return balance;
    }

    public String getAccountNode() {
        return accountNode;
    }
}
