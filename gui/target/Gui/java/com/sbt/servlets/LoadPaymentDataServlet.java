package com.sbt.servlets;

import com.sbt.dto.payments.PaymentDocumentRaw;
import com.sbt.servlets.common.JSPPageName;
import com.sbt.servlets.common.ServletName;
import com.sbt.servlets.common.ServletWithController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Vladimir Aseev on 24.03.2017.
 */
public class LoadPaymentDataServlet extends ServletWithController {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.getRequestDispatcher(JSPPageName.
                LOAD_PAYMENT_DATA.getJspName())
                .forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        req.setCharacterEncoding("UTF-8");

        PaymentDocumentRaw paymentDocumentRaw =
                readPaymentDocumentFromRequest(req);

        getGuiController()
                .savePaymentDocument(paymentDocumentRaw.getDocumentName(),
                        paymentDocumentRaw.getRawDocumentData());

        resp.sendRedirect(ServletName
                .SHOW_PAYMENT_DOCUMENTS_STATUS
                .getServletAddress());

    }
}
