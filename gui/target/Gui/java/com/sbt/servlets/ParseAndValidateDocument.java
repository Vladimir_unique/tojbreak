package com.sbt.servlets;

import com.sbt.dto.PaymentDocumentStatus;
import com.sbt.servlets.common.JSPPageName;
import com.sbt.servlets.common.ServletName;
import com.sbt.servlets.common.ServletWithController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Vladimir Aseev on 25.03.2017.
 */
public class ParseAndValidateDocument extends ServletWithController {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setAttribute("documents",
                getGuiController()
                        .getPaymentDocuments(PaymentDocumentStatus.SAVED));


        req.getRequestDispatcher(JSPPageName.
                START_DOCUMENT_PARSING_AND_VALIDATION.getJspName())
                .forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");

        String documentName = req.getParameter("paymentDocumentName");

        getGuiController().parseAndValidateDocument(documentName);

        resp.sendRedirect(ServletName
                .SHOW_PAYMENT_DOCUMENTS_STATUS
                .getServletAddress());

    }
}
