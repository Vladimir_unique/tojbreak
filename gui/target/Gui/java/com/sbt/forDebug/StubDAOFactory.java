package com.sbt.forDebug;

import com.sbt.daoInterfaces.*;

/**
 * Created by Vladimir Aseev on 24.03.2017.
 */
public class StubDAOFactory implements DAOFactory {

    private final StubClientDAO CLIENT_DAO;
    private final StubAccountDAO ACCOUNT_DAO;
    private final StubDocumentDAO DOCUMENT_DAO;

    public StubDAOFactory(){
        CLIENT_DAO = new StubClientDAO();
        ACCOUNT_DAO = new StubAccountDAO();
        DOCUMENT_DAO = new StubDocumentDAO();
    }


    @Override
    public AccountDAO getAccountDAO() {
        return ACCOUNT_DAO;
    }

    @Override
    public ClientDAO getClientDAO() {
        return CLIENT_DAO;
    }

    @Override
    public PaymentDocumentDAO getPaymentDocumentDAO() {
        return DOCUMENT_DAO;
    }

    @Override
    public PaymentRecordDAO getPaymentRecordDAO() {
        return null;
    }

    @Override
    public PaymentDocumentPartDAO getPaymentDocumentPartDAO() {
        return null;
    }
}
