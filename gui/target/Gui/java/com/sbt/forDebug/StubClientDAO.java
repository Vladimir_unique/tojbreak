package com.sbt.forDebug;

import com.sbt.daoInterfaces.ClientDAO;
import com.sbt.daoInterfaces.NodeName;
import com.sbt.dto.ClientDTO;
import com.sbt.dto.MethodResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vladimir Aseev on 24.03.2017.
 */
public class StubClientDAO implements ClientDAO {

    private final Map<String, ClientDTO> byCredentials;
    private final Map<Long, ClientDTO> byClientId;


    public StubClientDAO(){
        byCredentials = new HashMap<>();
        byClientId = new HashMap<>();
    }


    @Override
    public MethodResult<ClientDTO> createClient(String firstName,
                                                     String lastName,
                                                     String middleName,
                                                     boolean openNewTransaction) {
        long clientId = Math.round(Math.random() * Long.MAX_VALUE);
        String concatKey = firstName + lastName + middleName;
        ClientDTO addingClient = new ClientDTO(clientId,
                firstName,
                lastName,
                middleName,
                NodeName.GREEN);

        byClientId.put(clientId, addingClient);
        byCredentials.put(concatKey, addingClient);

        return MethodResult.ok(addingClient);
    }


    @Override
    public MethodResult<List<ClientDTO>> getClientByCredentials(String firstName,
                                                          String lastName,
                                                          String middleName,
                                                          boolean openNewTransaction) {
        String concatKey = firstName + lastName + middleName;
        if(byCredentials.containsKey(concatKey)){
            List<ClientDTO> result = new ArrayList<>();
            result.add(byCredentials.get(concatKey));
            return MethodResult.ok(result);
        } else {
            return MethodResult.onNotFound("Client not found for credentials: "
            + firstName + " "
            + lastName + " "
            + middleName);
        }

    }

    @Override
    public MethodResult<ClientDTO> getClientById(long clientId) {
        if(byClientId.containsKey(clientId)){
            return MethodResult.ok(byClientId.get(clientId));
        } else {
            return MethodResult.onNotFound("ClientDSO not found for id : " + clientId);
        }
    }

    @Override
    public MethodResult<List<ClientDTO>> getAll(boolean openNewTransaction) {
        List<ClientDTO> list = new ArrayList<>(byClientId.size());
        for(Map.Entry<Long, ClientDTO> entry : byClientId.entrySet()){
            list.add(entry.getValue());
        }
        return MethodResult.ok(list);
    }
}
