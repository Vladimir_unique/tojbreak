package com.sbt.forDebug;

import com.sbt.daoInterfaces.DAOFactory;
import com.sbt.dto.MethodResult;
import com.sbt.dto.PaymentDocumentStatus;
import com.sbt.services.PaymentsService;

/**
 * Created by Vladimir Aseev on 25.03.2017.
 */
public class StubPaymentsService implements PaymentsService {


    private final DAOFactory stubDAOFactory;

    public StubPaymentsService(DAOFactory stubDAOFactory){
        this.stubDAOFactory = stubDAOFactory;
    }


    @Override
    public MethodResult<PaymentDocumentStatus> saveNewPaymentsDocument(String paymentsDocumentName,
                                                                       String paymentsDocumentBinary) {
        MethodResult<PaymentDocumentStatus> rs = MethodResult.ok(stubDAOFactory
                .getPaymentDocumentDAO()
                .createDocument(paymentsDocumentName, 1, true)
                .getResult()
                .getStatus());

        return MethodResult.ok(stubDAOFactory
                .getPaymentDocumentDAO()
                .updateStatus(paymentsDocumentName,
                        PaymentDocumentStatus.SAVED,
                        true)
                .getResult()
                .getStatus());

    }

    @Override
    public MethodResult<PaymentDocumentStatus> parsePaymentsDocument(String paymentsDocumentName) {

        stubDAOFactory.getPaymentDocumentDAO().updateStatus(paymentsDocumentName,
                PaymentDocumentStatus.PARSED,
                true);

        return MethodResult.ok(PaymentDocumentStatus.PARSED);
    }

    @Override
    public MethodResult<PaymentDocumentStatus> validatePaymentsDocument(String paymentsDocumentName) {

        stubDAOFactory.getPaymentDocumentDAO().updateStatus(paymentsDocumentName,
                PaymentDocumentStatus.VALIDATED,
                true);

        return MethodResult.ok(PaymentDocumentStatus.VALIDATED);

    }

    @Override
    public MethodResult<PaymentDocumentStatus> startPaymentProcessForDocument(String paymentsDocumentName) {

        stubDAOFactory.getPaymentDocumentDAO().updateStatus(paymentsDocumentName,
                PaymentDocumentStatus.PAYMENTS_FINISHED,
                true);




        return MethodResult.ok(PaymentDocumentStatus.PAYMENTS_FINISHED);


    }
}
