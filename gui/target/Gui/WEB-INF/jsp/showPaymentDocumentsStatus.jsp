<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <title>Статус обработки платёжных документов</title>
</head>
<body>

<div class="container">
    <h1>Информация по статусу обработки платёжных документов</h1>
    <div class="col-sm-10">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Имя платёжного документа</th>
                <th>Количество блоков в документе</th>
                <th>Статус обработки</th>
                <th>Узел кластера</th>
            </tr>
            </thead>

            <c:forEach items="${documents}" var="document">

                <jsp:useBean id="document" scope="page" type="com.sbt.dto.PaymentDocumentDTO"/>

                <tr>
                    <td><c:out value="${document.documentName}"/></td>
                    <td><c:out value="${document.documentPartsCount}"/></td>
                    <td><c:out value="${document.status.textForGui}"/></td>
                    <td><c:out value="${document.storageNodeName.name()}"/></td>
                </tr>

            </c:forEach>
        </table>
    </div>
</div>

<div class="container">
    <div class="hyperlink">
        <a href="mainForm">На главную страницу</a>
    </div>
    <br>
</div>

</body>
</html>