<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <title>Зачисление по платёжным документам</title>
</head>
<body>

<div class="container">
    <h1>Выберите платёжный документ для проведения зачисления</h1>
    <div class="col-sm-10">
        <c:if test="${fn:length(documents) gt 0}">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Имя платёжного документа</th>
                    <th>Количество блоков в документе</th>
                    <th>Статус обработки</th>
                    <th>Узел кластера</th>
                </tr>
                </thead>

                <c:forEach items="${documents}" var="document">

                    <jsp:useBean id="document" scope="page" type="com.sbt.dto.PaymentDocumentDTO"/>

                    <tr onclick="startPaymentProcessForDocument(<c:out value="${document.documentName}"/>)">
                        <td><c:out value="${document.documentName}"/></td>
                        <td><c:out value="${document.documentPartsCount}"/></td>
                        <td><c:out value="${document.status.textForGui}"/></td>
                        <td><c:out value="${document.storageNodeName.name()}"/></td>
                    </tr>

                </c:forEach>
            </table>
        </c:if>
        <c:if test="${fn:length(documents) eq 0}">
            <div>
                <br>
                <p>Платёжные документы с подходящим статусом обработки отсутствуют</p>
                <br>
            </div>
        </c:if>
    </div>
</div>

<div class="container">
    <div class="hyperlink">
        <a href="mainForm">На главную страницу</a>
    </div>
    <br>
</div>

<form id="poster" action="startPaymentProcessing" method="POST">
    <input id="documentNameField" type="hidden" name="paymentDocumentName" value="undefined">
</form>

<script>
    function startPaymentProcessForDocument(documentName) {
        document.getElementById("documentNameField").value = documentName;
        document.getElementById("poster").submit();
    }
</script>

</body>
</html>