<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <title>Загрузка платёжного документа</title>
</head>
<body>

<div class="container">

    <form method="post" action="loadPaymentData" enctype="multipart/form-data" datatype="file">
        <h1>Загрузка файла платёжного документа</h1>
        <p>Задайте название платёжного документа. Это название будет использовано при обработке.</p>
        <input title="paymentDocumentName" type="text" name="paymentDocumentName" required/>
        <p>Выберите файл платёжки для первоначального сохранения в кластер.</p>
        <input type="file" name="file" required/>
        <input type="submit" value="Загрузить"/>
    </form>

</div>

<div class="container">
    <div class="hyperlink">
        <a href="mainForm">На главную страницу</a>
    </div>
    <p></p>
    <p></p>
</div>


</body>
</html>