<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <title>Данные платёжных записей в кластере</title>
</head>
<body>

<div class="container">
    <h1>Информация по платёжным записям</h1>
    <div class="col-sm-10">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>ID клиента</th>
                <th>Номер аккаунта</th>
                <th>Узел кластера клиента</th>
                <th>Узел кластера счёта</th>
                <th>Узел кластера записи</th>
                <th>Фамилия</th>
                <th>Имя</th>
                <th>Отчество</th>
                <th>Сумма зачисления</th>
            </tr>
            </thead>

            <c:forEach items="${paymentRecords}" var="paymentRecord">

                <jsp:useBean id="paymentRecord" scope="page" type="com.sbt.dto.paymentRecords.PaymentRecordObject"/>

                <tr>
                    <td><c:out value="${paymentRecord.clientId}"/></td>
                    <td><c:out value="${paymentRecord.accountNumber}"/></td>
                    <td><div id="rectangle1" style="width:40px; height:40px; background-color:<c:out value="${paymentRecord.clientNodeName}"/>"></div></td>
                    <td><div id="rectangle2" style="width:40px; height:40px; background-color:<c:out value="${paymentRecord.accountNodeName}"/>"></div></td>
                    <td><div id="rectangle3" style="width:40px; height:40px; background-color:<c:out value="${paymentRecord.recordNodeName}"/>"></div></td>
                    <td><c:out value="${paymentRecord.clientLastName}"/></td>
                    <td><c:out value="${paymentRecord.clientFirstName}"/></td>
                    <td><c:out value="${paymentRecord.clientMiddleName}"/></td>
                    <td><c:out value="${paymentRecord.paymentAmount}"/></td>
                </tr>

            </c:forEach>
        </table>
    </div>
</div>

<div class="container">
    <div class="hyperlink">
        <a href="mainForm">На главную страницу</a>
    </div>
    <br>
</div>

</body>
</html>