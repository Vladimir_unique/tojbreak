package com.sbt.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sbt.daoInterfaces.AccountDAO;
import com.sbt.daoInterfaces.ClientDAO;
import com.sbt.daoInterfaces.DAOFactory;
import com.sbt.daoInterfaces.PaymentRecordDAO;
import com.sbt.dto.*;
import com.sbt.dto.clientData.ClientDataObject;
import com.sbt.dto.paymentRecords.PaymentRecordObject;
import com.sbt.dto.payments.RawPaymentRecord;
import com.sbt.dto.tasks.*;
import com.sbt.dto.tasks.abstracts.GuiTask;
import com.sbt.dto.tasks.abstracts.TaskKind;
import com.sbt.services.PaymentsService;
import com.sbt.servlets.ShowPaymentRecordDistribution;

import java.util.*;

/**
 * Created by Vladimir Aseev on 22.03.2017.
 */
public class GuiController {

    private final Map<TaskKind, GuiTask> tasks;

    private static final Gson GSON;

    static {
        GsonBuilder builder = new GsonBuilder();
        GSON = builder
                .setPrettyPrinting()
                .create();
    }

    private final DAOFactory daoFactory;
    private final PaymentsService paymentsService;

    public GuiController(DAOFactory daoFactory,
                         PaymentsService paymentsService){
        tasks = new EnumMap<>(TaskKind.class);
        this.daoFactory = daoFactory;
        this.paymentsService = paymentsService;
        initMap();
    }

    private void initMap() {
        tasks.put(TaskKind.INIT_CLUSTER_DATA, new InitClusterData());
        tasks.put(TaskKind.LOAD_PAYMENT_DATA, new LoadPaymentData());
        tasks.put(TaskKind.SHOW_CLIENTS_DISTRIBUTION_AND_FUNDS,
                new ShowClientDistributionAndFunds());
        tasks.put(TaskKind.SHOW_PAYMENT_RECORDS_DISTRIBUTION,
                new ShowPaymentRecords());
        tasks.put(TaskKind.START_DOCUMENT_PARSING_AND_VALIDATION,
                new StartDocumentParsingAndValidation());
        tasks.put(TaskKind.START_PAYMENT_PROCESS,
                new StartPaymentProcess());
        tasks.put(TaskKind.SHOW_PAYMENT_DOCUMENTS_STATUS,
                new ShowPaymentDocumentsState());
    }

    public Collection<GuiTask> getAvailableOperations(){
        return tasks.values();
    }

    public void initClientsAndAccountsForPaymentDocument(String documentContent,
                                                         String documentName){
        List<RawPaymentRecord> paymentRecords =
                Arrays.asList(GSON.fromJson(documentContent,
                        RawPaymentRecord[].class));

        for(RawPaymentRecord processingRecord : paymentRecords){
            initDataForPaymentRecord(processingRecord, documentName);
        }

    }

    private void initDataForPaymentRecord(RawPaymentRecord record,
                                          String paymentDocumentName){

        ClientDAO clientDAO = daoFactory.getClientDAO();
        AccountDAO accountDAO = daoFactory.getAccountDAO();
        //PaymentRecordDAO paymentRecordDAO = daoFactory.getPaymentRecordDAO();

        MethodResult<ClientDTO> createClientResult =
                clientDAO.createClient(record.getClientFirstName(),
                        record.getClientLastName(),
                        record.getClientMiddleName(),
                        true);
        assert createClientResult
                .getResultStatus()
                .equals(MethodResultStatus.SUCCESS);
        assert createClientResult.getResult() != null;

        MethodResult<AccountDTO> createAccountResult =
                accountDAO.createAccount(record.getAccountNumber(),
                        createClientResult.getResult().getId(),
                        true);

        assert createAccountResult
                .getResultStatus()
                .equals(MethodResultStatus.SUCCESS);
        assert createAccountResult.getResult() != null;

        // debug only
/*        paymentRecordDAO.createRecord(paymentDocumentName,
                createClientResult.getResult().getId(),
                record.getAccountNumber(),
                record.getClientFirstName(),
                record.getClientLastName(),
                record.getClientMiddleName(),
                Math.round(record.getPaymentAmount()),
                true);*/


    }

    public Collection<PaymentRecordObject> getPaymentRecordsData(){
        PaymentRecordDAO paymentRecordDAO =
                daoFactory.getPaymentRecordDAO();

        MethodResult<List<PaymentRecordDTO>> allRecords =
                paymentRecordDAO.getAll(true);

        assert allRecords.getResultStatus()
                .equals(MethodResultStatus.SUCCESS);

        List<PaymentRecordObject> result =
                new ArrayList<>(allRecords.getResult().size());

        ClientDAO clientDAO = daoFactory.getClientDAO();
        AccountDAO accountDAO = daoFactory.getAccountDAO();

        for(PaymentRecordDTO record : allRecords.getResult()){
            String accountNumber = record.getAccountNumber();
            MethodResult<AccountDTO> accountByNumber = accountDAO
                    .getAccountByNumber(accountNumber, true);

            assert accountByNumber
                    .getResultStatus()
                    .equals(MethodResultStatus.SUCCESS);

            AccountDTO accountDTO = accountByNumber.getResult();

            MethodResult<ClientDTO> clientById =
                    clientDAO.getClientById(record.getClientId());

            assert clientById
                    .getResultStatus()
                    .equals(MethodResultStatus.SUCCESS);

            ClientDTO clientDTO = clientById.getResult();

            result.add(new PaymentRecordObject(String.valueOf(record.getClientId()),
                    accountNumber,
                    clientDTO.getStorageNodeName().name(),
                    accountDTO.getStorageNodeName().name(),
                    record.getStorageNodeName().name(),
                    record.getPaymentAmount(),
                    record.getClientFirstName(),
                    record.getClientLastName(),
                    record.getClientMiddleName()));

        }

        return result;
    }

    public Collection<ClientDataObject> getClientData(){

        ClientDAO clientDAO = daoFactory.getClientDAO();

        MethodResult<List<ClientDTO>> clientsResult =
                clientDAO.getAll(true);

        assert clientsResult.getResultStatus().equals(MethodResultStatus.SUCCESS);
        assert clientsResult.getResult() != null;

        List<ClientDataObject> result =
                new ArrayList<>(clientsResult.getResult().size());

        AccountDAO accountDAO = daoFactory.getAccountDAO();

        for(ClientDTO client : clientsResult.getResult()){
            long clientId = client.getId();
            MethodResult<List<AccountDTO>> accountResult =
                    accountDAO.getAccountsByClientId(clientId, true);

            assert accountResult.getResultStatus().equals(MethodResultStatus.SUCCESS);
            assert accountResult.getResult() != null;

            AccountDTO account = accountResult.getResult().get(0);

           result.add(mixIntoOneDto(client, account));
        }

        return result;
    }

    private ClientDataObject mixIntoOneDto(ClientDTO client, AccountDTO account){
        return new ClientDataObject(client.getId(),
                client.getStorageNodeName().name(),
                account.getStorageNodeName().name(),
                client.getFirstName(),
                client.getLastName(),
                client.getMiddleName(),
                account.getAccountNumber(),
                String.valueOf(account.getBalance()));
    }

    public void savePaymentDocument(String documentName, String documentBody){

        paymentsService.saveNewPaymentsDocument(documentName,
                documentBody);
    }

    public Collection<PaymentDocumentDTO> getPaymentDocuments(PaymentDocumentStatus onlyForStatus){

        MethodResult<List<PaymentDocumentDTO>> byStatus = daoFactory
                .getPaymentDocumentDAO()
                .getByStatus(onlyForStatus, true);

        assert !byStatus
                .getResultStatus()
                .equals(MethodResultStatus.ERROR);

        if(byStatus.getResult() != null && byStatus.getResult().size() != 0){
            return byStatus.getResult();
        } else {
            return new ArrayList<>();
        }
    }

    public Collection<PaymentDocumentDTO> getPaymentDocuments(){

        MethodResult<List<PaymentDocumentDTO>> byStatus = daoFactory
                .getPaymentDocumentDAO()
                .getAll(true);

        assert !byStatus
                .getResultStatus()
                .equals(MethodResultStatus.ERROR);

        if(byStatus.getResult() != null && byStatus.getResult().size() != 0){
            return byStatus.getResult();
        } else {
            return new ArrayList<>();
        }

    }


    public void parseAndValidateDocument(String documentName){

        paymentsService
                .parsePaymentsDocument(documentName);

        paymentsService
                .validatePaymentsDocument(documentName);

    }


    public void startPaymentProcessForDocument(String documentName){

        paymentsService
                .startPaymentProcessForDocument(documentName);

    }



}
