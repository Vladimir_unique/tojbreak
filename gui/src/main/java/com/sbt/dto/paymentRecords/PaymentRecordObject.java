package com.sbt.dto.paymentRecords;

/**
 * Created by Vladimir Aseev on 04.04.2017.
 */
public class PaymentRecordObject {

    private final String clientId;
    private final String accountNumber;
    private final String clientNodeName;
    private final String accountNodeName;
    private final String recordNodeName;
    private final long paymentAmount;
    private final String clientFirstName;
    private final String clientLastName;
    private final String clientMiddleName;


    public PaymentRecordObject(String clientId,
                               String accountNumber,
                               String clientNodeName,
                               String accountNodeName,
                               String recordNodeName,
                               long paymentAmount,
                               String clientFirstName,
                               String clientLastName,
                               String clientMiddleName) {
        this.clientId = clientId;
        this.accountNumber = accountNumber;
        this.clientNodeName = clientNodeName;
        this.accountNodeName = accountNodeName;
        this.recordNodeName = recordNodeName;
        this.paymentAmount = paymentAmount;
        this.clientFirstName = clientFirstName;
        this.clientLastName = clientLastName;
        this.clientMiddleName = clientMiddleName;
    }


    public String getClientId() {
        return clientId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getClientNodeName() {
        return clientNodeName;
    }

    public String getAccountNodeName() {
        return accountNodeName;
    }

    public String getRecordNodeName() {
        return recordNodeName;
    }

    public long getPaymentAmount() {
        return paymentAmount;
    }

    public String getClientFirstName() {
        return clientFirstName;
    }

    public String getClientLastName() {
        return clientLastName;
    }

    public String getClientMiddleName() {
        return clientMiddleName;
    }
}
