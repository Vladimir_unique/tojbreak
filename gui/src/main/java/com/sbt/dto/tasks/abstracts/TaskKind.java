package com.sbt.dto.tasks.abstracts;

/**
 * Created by Vladimir Aseev on 22.03.2017.
 *
 * Тип задачи, который может исполнять GUI
 */
public enum TaskKind {

    INIT_CLUSTER_DATA,
    SHOW_CLIENTS_DISTRIBUTION_AND_FUNDS,
    LOAD_PAYMENT_DATA,
    START_DOCUMENT_PARSING_AND_VALIDATION,
    START_PAYMENT_PROCESS,
    SHOW_PAYMENT_DOCUMENTS_STATUS,
    SHOW_PAYMENT_RECORDS_DISTRIBUTION

}
