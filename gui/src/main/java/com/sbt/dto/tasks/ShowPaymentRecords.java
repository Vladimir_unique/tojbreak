package com.sbt.dto.tasks;

import com.sbt.dto.tasks.abstracts.GuiTask;
import com.sbt.dto.tasks.abstracts.TaskKind;
import com.sbt.servlets.common.ServletName;

/**
 * Created by Vladimir Aseev on 04.04.2017.
 */
public class ShowPaymentRecords extends GuiTask {

    public ShowPaymentRecords() {
        super(TaskKind.SHOW_PAYMENT_RECORDS_DISTRIBUTION,
                "Платёжные записи",
                "Подробности по платёжным записям",
                ServletName
                        .SHOW_PAYMENT_RECORDS_DISTRIBUTION
                        .getServletAddress());
    }
}
