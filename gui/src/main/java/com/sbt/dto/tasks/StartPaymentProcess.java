package com.sbt.dto.tasks;

import com.sbt.dto.tasks.abstracts.GuiTask;
import com.sbt.dto.tasks.abstracts.TaskKind;
import com.sbt.servlets.common.JSPPageName;
import com.sbt.servlets.common.ServletName;

/**
 * Created by Vladimir Aseev on 22.03.2017.
 */
public class StartPaymentProcess extends GuiTask {


    public StartPaymentProcess() {
        super(TaskKind.START_PAYMENT_PROCESS,
                "Зачислить платёжку",
                "Начать процесс зачисления платёжки",
                ServletName.START_PAYMENT_PROCESSING.getServletAddress());
    }
}
