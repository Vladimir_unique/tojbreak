package com.sbt.servlets;

import com.sbt.dto.PaymentDocumentStatus;
import com.sbt.servlets.common.JSPPageName;
import com.sbt.servlets.common.ServletName;
import com.sbt.servlets.common.ServletWithController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Vladimir Aseev on 22.03.2017.
 */
public class StartPaymentProcess extends ServletWithController {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setAttribute("documents",
                getGuiController()
                        .getPaymentDocuments(PaymentDocumentStatus.VALIDATED));


        req.getRequestDispatcher(JSPPageName.
                START_PAYMENT_PROCESS.getJspName())
                .forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");

        String documentName = req.getParameter("paymentDocumentName");

        getGuiController().startPaymentProcessForDocument(documentName);

        resp.sendRedirect(ServletName
                .SHOW_PAYMENT_DOCUMENTS_STATUS
                .getServletAddress());

    }
}
