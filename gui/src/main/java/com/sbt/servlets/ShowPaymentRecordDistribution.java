package com.sbt.servlets;

import com.sbt.servlets.common.JSPPageName;
import com.sbt.servlets.common.ServletWithController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Vladimir Aseev on 04.04.2017.
 */
public class ShowPaymentRecordDistribution extends ServletWithController {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setAttribute("paymentRecords",
                getGuiController().getPaymentRecordsData());

        req.getRequestDispatcher(JSPPageName
                .SHOW_PAYMENT_RECORDS_DISTRIBUTION
                .getJspName())
                .forward(req, resp);

    }
}
