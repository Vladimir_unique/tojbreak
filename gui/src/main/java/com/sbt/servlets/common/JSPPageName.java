package com.sbt.servlets.common;

/**
 * Created by Vladimir Aseev on 22.03.2017.
 */
public enum JSPPageName {

    TASKS("WEB-INF/jsp/tasks.jsp"),
    INIT_CLUSTER_DATA("WEB-INF/jsp/initClusterData.jsp"),
    LOAD_PAYMENT_DATA("WEB-INF/jsp/loadPaymentData.jsp"),
    SHOW_CLIENT_DISTRIBUTION("WEB-INF/jsp/showClientDistribution.jsp"),
    SHOW_PAYMENT_DOCUMENTS_STATUS("WEB-INF/jsp/showPaymentDocumentsStatus.jsp"),
    START_DOCUMENT_PARSING_AND_VALIDATION("WEB-INF/jsp/startDocumentParsingAndValidation.jsp"),
    START_PAYMENT_PROCESS("WEB-INF/jsp/startPaymentProcess.jsp"),
    SHOW_PAYMENT_RECORDS_DISTRIBUTION("WEB-INF/jsp/showPaymentRecordsDistribution.jsp");



    final String jspName;

    JSPPageName(String jspName){
        this.jspName = jspName;
    }

    public String getJspName() {
        return jspName;
    }
}
