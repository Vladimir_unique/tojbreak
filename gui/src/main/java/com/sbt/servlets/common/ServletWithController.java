package com.sbt.servlets.common;

import com.sbt.controller.ClientContextHolder;
import com.sbt.controller.GuiController;
import com.sbt.daoInterfaces.DAOFactory;
import com.sbt.dto.payments.PaymentDocumentRaw;
import com.sbt.services.PaymentsService;
import org.springframework.context.ApplicationContext;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Enumeration;
import java.util.stream.Collectors;

/**
 * Created by Vladimir Aseev on 22.03.2017.
 */
public class ServletWithController extends HttpServlet {

    private static final GuiController GUI_CONTROLLER =
            new GuiController(ClientContextHolder
                    .getContext()
                    .getBean(DAOFactory.class),
                    ClientContextHolder
                            .getContext()
                            .getBean(PaymentsService.class));

    protected ServletWithController(){

    }

    protected PaymentDocumentRaw readPaymentDocumentFromRequest(HttpServletRequest req){

        String header = req.getHeader("Content-Type");

        String boundary = header.split("boundary=")[1];

        String documentName;
        String documentData;

        try{
            String[] split = req
                    .getReader()
                    .lines()
                    .skip(3)
                    .collect(Collectors.joining())
                    .split("--" + boundary);
            documentName = split[0];

            documentData = split[1]
                    .split("Content-Type: text/plain")[1]
                    .substring(1);

            System.out.println(documentData);

        } catch (IOException e){
            throw new RuntimeException("Could not parse request.", e);
        }

        return new PaymentDocumentRaw(documentName, documentData);
    }

    protected String readRawFileDataFromPostRequest(HttpServletRequest req){
/*                // prints out all header values
        System.out.println("===== Begin headers =====");
        Enumeration<String> names = req.getHeaderNames();
        while (names.hasMoreElements()) {
            String headerName = names.nextElement();
            System.out.println(headerName + " = " + req.getHeader(headerName));
        }
        System.out.println("===== End headers =====\n");*/

        String boundary =
                req.getHeader("Content-Type").split("boundary=")[1];
        try{
            return req
                    .getReader()
                    .lines()
                    .skip(3)
                    .collect(Collectors.joining())
                    .split("--" + boundary)[0];
        } catch (IOException e){
            return "";
        }

    }

    protected GuiController getGuiController(){
        return GUI_CONTROLLER;
    }

}
