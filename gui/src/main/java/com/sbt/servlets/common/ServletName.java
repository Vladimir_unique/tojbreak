package com.sbt.servlets.common;

/**
 * Created by Vladimir Aseev on 23.03.2017.
 */
public enum ServletName {

    INIT_CLUSTER_DATA("initClusterData"),
    LOAD_PAYMENT_DATA("loadPaymentData"),
    SHOW_CLIENT_DISTRIBUTION("showClientDistribution"),
    START_DOCUMENT_PARSING_AND_VALIDATION("startDocumentParsingAndValidation"),
    START_PAYMENT_PROCESSING("startPaymentProcessing"),
    SHOW_PAYMENT_DOCUMENTS_STATUS("showPaymentDocumentsStatus"),
    SHOW_PAYMENT_RECORDS_DISTRIBUTION("showPaymentRecordsDistribution");


    String servletAddress;

    ServletName(String servletAddress){
        this.servletAddress = servletAddress;
    }

    public String getServletAddress() {
        return servletAddress;
    }
}
