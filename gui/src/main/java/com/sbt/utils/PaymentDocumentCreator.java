package com.sbt.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sbt.dto.PaymentRecordDTO;
import com.sbt.dto.payments.RawPaymentRecord;

/**
 * Created by Vladimir Aseev on 23.03.2017.
 */
public class PaymentDocumentCreator {
    
    private static final Gson GSON;
    
    static {
        GsonBuilder builder = new GsonBuilder();
        GSON = builder
                .setPrettyPrinting()
                .create();
    }

    private static final RawPaymentRecord[] PAYMENTS;

    static {
        PAYMENTS = new RawPaymentRecord[21];
        PAYMENTS[0] = new RawPaymentRecord("810-001", "Владимир", "Кудинов", "Викторович", 100D);
        PAYMENTS[1] = new RawPaymentRecord("810-002", "Михаил", "Алексеев", "Антонович", 100D);
        PAYMENTS[2] = new RawPaymentRecord("810-003", "Станислав", "Королёв", "Михайлович", 100D);
        PAYMENTS[3] = new RawPaymentRecord("810-004", "Евгений", "Елисеев", "Дмитриевич", 100D);
        PAYMENTS[4] = new RawPaymentRecord("810-005", "Алексей", "Токарев", "Иванович", 100D);
        PAYMENTS[5] = new RawPaymentRecord("810-006", "Светлана", "Батько", "Романовна", 100D);
        PAYMENTS[6] = new RawPaymentRecord("810-007", "Джеймс", "Бонд", "Эндрю", 100D);
        PAYMENTS[7] = new RawPaymentRecord("810-008", "Алина", "Кондрашева", "Андреевна", 100D);
        PAYMENTS[8] = new RawPaymentRecord("810-009", "Роман", "Шарнин", "Евгеньевич", 100D);
        PAYMENTS[9] = new RawPaymentRecord("810-010", "Дмитрий", "Кудряшев", "Никитич", 100D);
        PAYMENTS[10] = new RawPaymentRecord("810-011", "Анастасия", "Нелепа", "Захаровна", 100D);
        PAYMENTS[11] = new RawPaymentRecord("810-012", "Валентина", "Волкова", "Алексеевна", 100D);
        PAYMENTS[12] = new RawPaymentRecord("810-013", "Георгий", "Ястребов", "Михайлович", 100D);
        PAYMENTS[13] = new RawPaymentRecord("810-014", "Фёдор", "Белько", "Анатольевич", 100D);
        PAYMENTS[14] = new RawPaymentRecord("810-015", "Кристина", "Фет", "Николаевна", 100D);
        PAYMENTS[15] = new RawPaymentRecord("810-016", "Виктор", "Куприянов", "Эдуардович", 100D);
        PAYMENTS[16] = new RawPaymentRecord("810-017", "Олег", "Золотов", "Александрович", 100D);
        PAYMENTS[17] = new RawPaymentRecord("810-018", "Евгения", "Кузина", "Сергеевна", 100D);
        PAYMENTS[18] = new RawPaymentRecord("810-019", "Яна", "Иванова", "Игоревна", 100D);
        PAYMENTS[19] = new RawPaymentRecord("810-020", "Донателло", "Ниндзя", "Черепахович", 100D);
        PAYMENTS[20] = new RawPaymentRecord("810-021", "Николай", "Романов", "Павлович", 100D);

    }


    private static String createPaymentDocumentJson(){
        return GSON.toJson(PAYMENTS);
    }


    public static void main(String[] args) {
        System.out.println(createPaymentDocumentJson());
    }
    
    
}
