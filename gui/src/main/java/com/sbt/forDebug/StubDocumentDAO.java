package com.sbt.forDebug;

import com.sbt.daoInterfaces.NodeName;
import com.sbt.daoInterfaces.PaymentDocumentDAO;
import com.sbt.dto.MethodResult;
import com.sbt.dto.PaymentDocumentDTO;
import com.sbt.dto.PaymentDocumentStatus;

import java.util.*;

/**
 * Created by Vladimir Aseev on 25.03.2017.
 */
public class StubDocumentDAO implements PaymentDocumentDAO {

    private final Map<String, PaymentDocumentDTO> byName;
    private final EnumMap<PaymentDocumentStatus, List<PaymentDocumentDTO>> byStatus;


    public StubDocumentDAO(){
        this.byName = new HashMap<>();
        this.byStatus = new EnumMap<>(PaymentDocumentStatus.class);
    }


    @Override
    public MethodResult<PaymentDocumentDTO> createDocument(String documentName,
                                                           int documentPartsCount,
                                                           boolean openNewTransaction) {
        if(byName.containsKey(documentName)){
            return MethodResult.ok(byName.get(documentName));
        } else {
            PaymentDocumentDTO addingDocument =
                    new PaymentDocumentDTO(documentName,
                            PaymentDocumentStatus.SAVED,
                            NodeName.GREEN,
                            documentPartsCount);

            byName.put(documentName, addingDocument);
            if(!byStatus.containsKey(PaymentDocumentStatus.SAVED)){
                byStatus.put(PaymentDocumentStatus.SAVED, new ArrayList<>());
            }

            byStatus.get(PaymentDocumentStatus.SAVED).add(addingDocument);

            return MethodResult.ok(addingDocument);
        }

    }

    @Override
    public MethodResult<PaymentDocumentDTO> getByName(String documentName, boolean openNewTransaction) {

        if(byName.containsKey(documentName)){
            return MethodResult.ok(byName.get(documentName));
        } else {
            return MethodResult
                    .onNotFound("Payment document not found for name: " + documentName);
        }

    }

    @Override
    public MethodResult<List<PaymentDocumentDTO>> getByStatus(PaymentDocumentStatus status, boolean openNewTransaction) {

        if(byStatus.containsKey(status)){
            return MethodResult.ok(byStatus.get(status));
        } else {
            return MethodResult
                    .onNotFound("Payment documents not found for status: "
                            + status.name());
        }
    }

    @Override
    public MethodResult<PaymentDocumentDTO> updateStatus(String documentName,
                                                         PaymentDocumentStatus status,
                                                         boolean openNewTransaction) {

        if(!byName.containsKey(documentName)){
            return MethodResult
                    .onNotFound("Payment document not found for name : " + documentName);
        }


        PaymentDocumentDTO oldVersion = byName.get(documentName);
        PaymentDocumentDTO newVersion = new PaymentDocumentDTO(
                documentName,
                status,
                NodeName.GREEN,
                oldVersion.getDocumentPartsCount());

        byName.replace(documentName, newVersion);

        List<PaymentDocumentDTO> byStatusListOld =
                byStatus.get(oldVersion.getStatus());

        byStatusListOld.remove(oldVersion);

        if(!byStatus.containsKey(status)){
            byStatus.put(status, new ArrayList<>());
        }


        List<PaymentDocumentDTO> byStatusListNew =
                byStatus.get(status);

        byStatusListNew.add(newVersion);

        return MethodResult.ok(newVersion);
    }

    @Override
    public MethodResult<List<PaymentDocumentDTO>> getAll(boolean openNewTransaction) {

        List<PaymentDocumentDTO> list = new ArrayList<>(byName.size());
        for(Map.Entry<String, PaymentDocumentDTO> entry : byName.entrySet()){
            list.add(entry.getValue());
        }

        return MethodResult.ok(list);
    }
}
