package com.sbt.forDebug;

import com.sbt.daoInterfaces.AccountDAO;
import com.sbt.daoInterfaces.NodeName;
import com.sbt.dto.AccountDTO;
import com.sbt.dto.MethodResult;

import java.util.*;

/**
 * Created by Vladimir Aseev on 24.03.2017.
 */
public class StubAccountDAO implements AccountDAO {

    private final Map<String, AccountDTO> byAccountNumber;
    private final Map<Long, AccountDTO> byClientId;

    public StubAccountDAO(){
        byAccountNumber = new HashMap<>();
        byClientId = new HashMap<>();
    }

    @Override
    public MethodResult<AccountDTO> createAccount(String accountNumber,
                                                  long clientId,
                                                  boolean openNewTransaction) {
        AccountDTO creatingAccount =
                new AccountDTO(Math.round(Math.random() * Long.MAX_VALUE),
                        clientId,
                        accountNumber,
                        0L,
                        NodeName.GREEN);

        byAccountNumber.put(accountNumber, creatingAccount);
        byClientId.put(clientId, creatingAccount);

        return MethodResult.ok(creatingAccount);
    }

    @Override
    public MethodResult<List<AccountDTO>> getAccountsByClientId(long clientId, boolean openNewTransaction) {
        if(byClientId.containsKey(clientId)){
            List<AccountDTO> result = new ArrayList<>();
            result.add(byClientId.get(clientId));
            return MethodResult.ok(result);
        } else {
            return MethodResult
                    .onNotFound("AccountDSO not found for clientId: " + clientId);
        }
    }



    @Override
    public MethodResult<AccountDTO> getAccountByNumber(String accountNumber, boolean openNewTransaction) {
        if(byAccountNumber.containsKey(accountNumber)){
            return MethodResult.ok(byAccountNumber.get(accountNumber));
        } else {
            return MethodResult
                    .onNotFound("AccountDSO not found for account number: " +
            accountNumber);
        }
    }

    @Override
    public MethodResult<List<AccountDTO>> getAll(boolean openNewTransaction) {
        List<AccountDTO> list = new ArrayList<>(byAccountNumber.size());
        for(Map.Entry<String, AccountDTO> entry : byAccountNumber.entrySet()){
            list.add(entry.getValue());
        }
        return MethodResult.ok(list);
    }


    @Override
    public MethodResult<AccountDTO> income(String accountNumber, long amount, boolean openNewTransaction) {
        if(byAccountNumber.containsKey(accountNumber)){
            AccountDTO accountDTO = byAccountNumber.get(accountNumber);
            long balanceWas = accountDTO.getBalance();
            long balanceNew = balanceWas + amount;
            AccountDTO newDto = new AccountDTO(accountDTO.getId(),
                    accountDTO.getClientId(),
                    accountNumber,
                    balanceNew,
                    NodeName.GREEN);
            byAccountNumber.replace(accountNumber, newDto);
            byClientId.replace(accountDTO.getClientId(), newDto);

            return MethodResult.ok(newDto);
        } else {
            return MethodResult
                    .onNotFound("AccountDSO not found for number : " + accountNumber);
        }
    }
}
