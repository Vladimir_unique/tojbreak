<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <title>Список задач</title>
</head>
<body>

<div class="container">
    <h1>Список задач</h1>
    <p>Выберите задачу для кластера Ignite</p>
    <div class="col-sm-10">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Название задачи</th>
                <th>Описание задачи</th>
            </tr>
            </thead>

            <c:forEach items="${guiTasks}" var="guiTask">

                <jsp:useBean id="guiTask" scope="page" type="com.sbt.dto.tasks.abstracts.GuiTask"/>

                <tr>
                    <td><a href="${guiTask.taskProviderRelativePath}">${guiTask.taskVisibleName}</a></td>

                    <td><c:out value="${guiTask.taskVisibleDescription}"/></td>

                </tr>

            </c:forEach>
        </table>
    </div>
</div>

</body>
</html>