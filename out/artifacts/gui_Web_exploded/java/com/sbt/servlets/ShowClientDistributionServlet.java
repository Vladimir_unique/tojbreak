package com.sbt.servlets;

import com.sbt.servlets.common.JSPPageName;
import com.sbt.servlets.common.ServletWithController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Vladimir Aseev on 22.03.2017.
 */
public class ShowClientDistributionServlet extends ServletWithController {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setAttribute("clients",
                getGuiController().getClientData());

        req.getRequestDispatcher(JSPPageName
                .SHOW_CLIENT_DISTRIBUTION
                .getJspName())
                .forward(req, resp);

    }
}
