package com.sbt.servlets;

import com.sbt.servlets.common.ServletWithController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Vladimir Aseev on 21.03.2017.
 */

public class MainFormServlet extends ServletWithController {


    public MainFormServlet(){
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setAttribute("guiTasks",
                getGuiController().getAvailableOperations());
        req.getRequestDispatcher("/tasks.jsp").forward(req, resp);

    }


}
