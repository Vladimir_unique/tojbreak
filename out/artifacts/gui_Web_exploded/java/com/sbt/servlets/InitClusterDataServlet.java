package com.sbt.servlets;

import com.sbt.servlets.common.ServletName;
import com.sbt.servlets.common.ServletWithController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Vladimir Aseev on 22.03.2017.
 */
public class InitClusterDataServlet extends ServletWithController {


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");

        String fileData =
                readRawFileDataFromPostRequest(req);

        getGuiController()
                .initClientsAndAccountsForPaymentDocument(fileData);

        resp.sendRedirect(ServletName
                .SHOW_CLIENT_DISTRIBUTION
                .getServletAddress());

    }


}
