package com.sbt.servlets.common;

/**
 * Created by Vladimir Aseev on 22.03.2017.
 */
public enum JSPPageName {


    INIT_CLUSTER_DATA("initClusterData.jsp"),
    LOAD_PAYMENT_DATA("loadPaymentData.jsp"),
    SHOW_CLIENT_DISTRIBUTION("showClientDistribution.jsp"),
    START_DOCUMENT_PARSING_AND_VALIDATION("startDocumentParsingAndValidation.jsp"),
    START_PAYMENT_PROCESS("startPaymentProcess.jsp");



    final String jspName;

    JSPPageName(String jspName){
        this.jspName = jspName;
    }

    public String getJspName() {
        return jspName;
    }
}
