package com.sbt.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sbt.daoInterfaces.AccountDAO;
import com.sbt.daoInterfaces.ClientDAO;
import com.sbt.daoInterfaces.DAOFactory;
import com.sbt.dto.AccountDTO;
import com.sbt.dto.ClientDTO;
import com.sbt.dto.MethodResult;
import com.sbt.dto.MethodResultStatus;
import com.sbt.dto.clientData.ClientDataObject;
import com.sbt.dto.payments.RawPaymentRecord;
import com.sbt.dto.tasks.*;
import com.sbt.dto.tasks.abstracts.GuiTask;
import com.sbt.dto.tasks.abstracts.TaskKind;

import java.util.*;

/**
 * Created by Vladimir Aseev on 22.03.2017.
 */
public class GuiController {

    private final Map<TaskKind, GuiTask> tasks;

    private static final Gson GSON;

    static {
        GsonBuilder builder = new GsonBuilder();
        GSON = builder
                .setPrettyPrinting()
                .create();
    }

    private final DAOFactory daoFactory;

    public GuiController(DAOFactory daoFactory){
        tasks = new EnumMap<>(TaskKind.class);
        this.daoFactory = daoFactory;
        initMap();
    }

    private void initMap() {
        tasks.put(TaskKind.INIT_CLUSTER_DATA, new InitClusterData());
        tasks.put(TaskKind.LOAD_PAYMENT_DATA, new LoadPaymentData());
        tasks.put(TaskKind.SHOW_CLIENTS_DISTRIBUTION_AND_FUNDS,
                new ShowClientDistributionAndFunds());
        tasks.put(TaskKind.START_DOCUMENT_PARSING_AND_VALIDATION,
                new StartDocumentParsingAndValidation());
        tasks.put(TaskKind.START_PAYMENT_PROCESS,
                new StartPaymentProcess());
        tasks.put(TaskKind.SHOW_PAYMENT_DOCUMENTS_STATUS,
                new ShowPaymentDocumentsState());
    }

    public Collection<GuiTask> getAvailableOperations(){
        return tasks.values();
    }

    public void initClientsAndAccountsForPaymentDocument(String documentContent){
        List<RawPaymentRecord> paymentRecords =
                Arrays.asList(GSON.fromJson(documentContent,
                        RawPaymentRecord[].class));

        for(RawPaymentRecord processingRecord : paymentRecords){
            initDataForPaymentRecord(processingRecord);
        }

    }

    private void initDataForPaymentRecord(RawPaymentRecord record){

        ClientDAO clientDAO = daoFactory.getClientDAO();
        AccountDAO accountDAO = daoFactory.getAccountDAO();

        MethodResult<ClientDTO> createClientResult =
                clientDAO.createClientOrGet(record.getClientFirstName(),
                        record.getClientLastName(),
                        record.getClientMiddleName(),
                        true);
        assert createClientResult
                .getResultStatus()
                .equals(MethodResultStatus.SUCCESS);
        assert createClientResult.getResult() != null;

        MethodResult<AccountDTO> createAccountResult =
                accountDAO.createAccountOrGet(record.getAccountNumber(),
                        createClientResult.getResult().getId(),
                        true);

        assert createAccountResult
                .getResultStatus()
                .equals(MethodResultStatus.SUCCESS);
        assert createAccountResult.getResult() != null;
    }

    public Collection<ClientDataObject> getClientData(){

        ClientDAO clientDAO = daoFactory.getClientDAO();

        MethodResult<List<ClientDTO>> clientsResult =
                clientDAO.getAll(true);

        assert clientsResult.getResultStatus().equals(MethodResultStatus.SUCCESS);
        assert clientsResult.getResult() != null;

        List<ClientDataObject> result =
                new ArrayList<>(clientsResult.getResult().size());

        AccountDAO accountDAO = daoFactory.getAccountDAO();

        for(ClientDTO client : clientsResult.getResult()){
            long clientId = client.getId();
            MethodResult<AccountDTO> accountResult =
                    accountDAO.getAccountByClientId(clientId, true);

            assert accountResult.getResultStatus().equals(MethodResultStatus.SUCCESS);
            assert accountResult.getResult() != null;

            AccountDTO account = accountResult.getResult();

           result.add(mixIntoOneDto(client, account));
        }

        return result;
    }

    private ClientDataObject mixIntoOneDto(ClientDTO client, AccountDTO account){
        return new ClientDataObject(client.getId(),
                client.getStorageNodeName(),
                client.getFirstName(),
                client.getLastName(),
                client.getMiddleName(),
                account.getAccountNumber(),
                String.valueOf(account.getBalance()));
    }



}
