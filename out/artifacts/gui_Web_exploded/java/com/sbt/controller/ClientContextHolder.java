package com.sbt.controller;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Vladimir Aseev on 24.03.2017.
 */
public class ClientContextHolder {

    private static ClassPathXmlApplicationContext SPRING_CONTEXT =
            new ClassPathXmlApplicationContext("gui-context.xml");


    private ClientContextHolder(){

    }

    public static ClassPathXmlApplicationContext getContext(){
        return SPRING_CONTEXT;
    }


}
