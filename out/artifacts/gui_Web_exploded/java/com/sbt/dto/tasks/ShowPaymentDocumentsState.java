package com.sbt.dto.tasks;

import com.sbt.dto.tasks.abstracts.GuiTask;
import com.sbt.dto.tasks.abstracts.TaskKind;
import com.sbt.servlets.common.ServletName;

/**
 * Created by Vladimir Aseev on 23.03.2017.
 */
public class ShowPaymentDocumentsState extends GuiTask {


    public ShowPaymentDocumentsState() {
        super(TaskKind.SHOW_PAYMENT_DOCUMENTS_STATUS,
                "Платёжные документы",
                "Показать статус платёжных документов",
                ServletName.SHOW_PAYMENT_DOCUMENTS_STATUS.getServletAddress());
    }
}
