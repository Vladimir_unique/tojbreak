package com.sbt.dto.tasks;

import com.sbt.dto.tasks.abstracts.GuiTask;
import com.sbt.dto.tasks.abstracts.TaskKind;
import com.sbt.servlets.common.JSPPageName;

/**
 * Created by Vladimir Aseev on 22.03.2017.
 */
public class LoadPaymentData extends GuiTask {

    public LoadPaymentData() {
        super(TaskKind.LOAD_PAYMENT_DATA,
                "Загрузить платёжку",
                "Загрузить платёжные данные",
                JSPPageName.LOAD_PAYMENT_DATA.getJspName());
    }
}
