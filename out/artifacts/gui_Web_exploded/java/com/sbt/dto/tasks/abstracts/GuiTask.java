package com.sbt.dto.tasks.abstracts;

/**
 * Created by Vladimir Aseev on 22.03.2017.
 */
public abstract class GuiTask {

    private final TaskKind taskKind;
    private final String taskVisibleName;
    private final String taskVisibleDescription;
    private final String taskProviderRelativePath;


    public GuiTask(TaskKind taskKind, String taskVisibleName, String taskVisibleDescription, String taskProviderRelativePath){
        this.taskKind = taskKind;
        this.taskVisibleName = taskVisibleName;
        this.taskVisibleDescription = taskVisibleDescription;
        this.taskProviderRelativePath = taskProviderRelativePath;
    }


    public TaskKind getTaskKind() {
        return taskKind;
    }

    public String getTaskVisibleName() {
        return taskVisibleName;
    }

    public String getTaskVisibleDescription() {
        return taskVisibleDescription;
    }

    public String getTaskProviderRelativePath() {
        return taskProviderRelativePath;
    }
}
