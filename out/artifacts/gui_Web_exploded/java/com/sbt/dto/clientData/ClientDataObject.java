package com.sbt.dto.clientData;

/**
 * Created by Vladimir Aseev on 23.03.2017.
 */
public class ClientDataObject {

    private final long id;
    private final String node;
    private final String firstName;
    private final String lastName;
    private final String middleName;
    private final String accountNumber;
    private final String balance;

    public ClientDataObject(long id,
                            String node,
                            String firstName,
                            String lastName,
                            String middleName,
                            String accountNumber,
                            String balance) {
        this.id = id;
        this.node = node;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.accountNumber = accountNumber;
        this.balance = balance;
    }

    public long getId() {
        return id;
    }

    public String getNode() {
        return node;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getBalance() {
        return balance;
    }
}
