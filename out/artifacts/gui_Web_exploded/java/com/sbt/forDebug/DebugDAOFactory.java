package com.sbt.forDebug;

import com.sbt.daoInterfaces.*;
import com.sbt.dto.clientData.ClientDataObject;

/**
 * Created by Vladimir Aseev on 24.03.2017.
 */
public class DebugDAOFactory implements DAOFactory {


    private final DebugClientDAO CLIENT_DAO;
    private final DebugAccountDAO ACCOUNT_DAO;

    public DebugDAOFactory(){
        CLIENT_DAO = new DebugClientDAO();
        ACCOUNT_DAO = new DebugAccountDAO();
    }


    @Override
    public AccountDAO getAccountDAO() {
        return ACCOUNT_DAO;
    }

    @Override
    public ClientDAO getClientDAO() {
        return CLIENT_DAO;
    }

    @Override
    public PaymentDocumentDAO getPaymentDocumentDAO() {
        return null;
    }

    @Override
    public PaymentRecordDAO getPaymentRecordDAO() {
        return null;
    }
}
