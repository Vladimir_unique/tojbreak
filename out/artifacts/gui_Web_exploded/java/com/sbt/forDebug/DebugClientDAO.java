package com.sbt.forDebug;

import com.sbt.daoInterfaces.ClientDAO;
import com.sbt.dto.ClientDTO;
import com.sbt.dto.MethodResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vladimir Aseev on 24.03.2017.
 */
public class DebugClientDAO implements ClientDAO {

    private final Map<String, ClientDTO> byCredentials;
    private final Map<Long, ClientDTO> byClientId;


    public DebugClientDAO(){
        byCredentials = new HashMap<>();
        byClientId = new HashMap<>();
    }


    @Override
    public MethodResult<ClientDTO> createClientOrGet(String firstName,
                                                     String lastName,
                                                     String middleName,
                                                     boolean openNewTransaction) {
        long clientId = Math.round(Math.random() * Long.MAX_VALUE);
        String concatKey = firstName + lastName + middleName;
        String nodeId = "debug_node";
        ClientDTO addingClient = new ClientDTO(clientId,
                firstName,
                lastName,
                middleName,
                nodeId);

        byClientId.put(clientId, addingClient);
        byCredentials.put(concatKey, addingClient);

        return MethodResult.ok(addingClient);
    }

    @Override
    public MethodResult<ClientDTO> getClientByCredentials(String firstName,
                                                          String lastName,
                                                          String middleName,
                                                          boolean openNewTransaction) {
        String concatKey = firstName + lastName + middleName;
        if(byCredentials.containsKey(concatKey)){
            return MethodResult.ok(byCredentials.get(concatKey));
        } else {
            return MethodResult.onNotFound("Client not found for credentials: "
            + firstName + " "
            + lastName + " "
            + middleName);
        }

    }

    @Override
    public MethodResult<ClientDTO> getClientById(long clientId) {
        if(byClientId.containsKey(clientId)){
            return MethodResult.ok(byClientId.get(clientId));
        } else {
            return MethodResult.onNotFound("Client not found for id : " + clientId);
        }
    }

    @Override
    public MethodResult<List<ClientDTO>> getAll(boolean openNewTransaction) {
        List<ClientDTO> list = new ArrayList<>(byClientId.size());
        for(Map.Entry<Long, ClientDTO> entry : byClientId.entrySet()){
            list.add(entry.getValue());
        }
        return MethodResult.ok(list);
    }
}
