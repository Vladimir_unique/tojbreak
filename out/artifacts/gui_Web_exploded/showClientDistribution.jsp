<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <title>Данные клиентов в кластере</title>
</head>
<body>

<div class="container">
    <h1>Информация по клиентам и счетам</h1>
    <div class="col-sm-10">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>ID клиента</th>
                <th>Узел кластера</th>
                <th>Фамилия</th>
                <th>Имя</th>
                <th>Отчество</th>
                <th>Номер счёта</th>
                <th>Баланс</th>
            </tr>
            </thead>

            <c:forEach items="${clients}" var="clientDSO">

                <jsp:useBean key="clientDSO" scope="page" type="com.sbt.dto.clientData.ClientDataObject"/>

                <tr>
                    <td><c:out value="${clientDSO.key}"/></td>
                    <td><c:out value="${clientDSO.clientNode}"/></td>
                    <td><c:out value="${clientDSO.lastName}"/></td>
                    <td><c:out value="${clientDSO.firstName}"/></td>
                    <td><c:out value="${clientDSO.middleName}"/></td>
                    <td><c:out value="${clientDSO.paymentAccountNumber}"/></td>
                    <td><c:out value="${clientDSO.balance}"/></td>
                </tr>

            </c:forEach>
        </table>
    </div>
</div>

<div class="container">
    <div class="hyperlink">
        <a href="mainForm">На главную страницу</a>
    </div>
    <br>
</div>

</body>
</html>